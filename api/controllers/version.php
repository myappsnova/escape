<?php
class vers extends apiController{

	/**
	* @url GET /version
	* @noAuth
	*/
	public function getVersion()
	{
		
		if (getenv('CI_COMMIT_TAG')) {
			$version = getenv('CI_COMMIT_TAG');
		}else {
			$version = 'localhost';
		}

		return $version;
	}

}
