<?php

class auth extends apiController{

	/**
	* @url GET /login/$login/$password
	* @noAuth
	*/
	public function log($login,$password)
	{
		$bdd = new BDD(); // Get uuid();

		$reponse = $bdd->access()->prepare('SELECT * FROM users WHERE mail=?');
		$one = $login;
		$reponse->bindParam(1,$one);
		$reponse->execute();
		$result = $reponse->fetch();
		$r = json_decode(json_encode($result));
		//get account

		// Check if user exists
		if (!$r)
		{
		// throw
			throw new Jacwright\RestServer\RestException(406, 'Incorrect Login');
		}

		// Check if password matches the user password
		if (!hash_equals($r->pass, crypt($password, $r->pass)))
		{
		// check old version
			if (sha1($password) != $r->pass)
			{
		// throw
				throw new Jacwright\RestServer\RestException(406, 'Incorrect Password');
			}
		}

		// Generate temporary app id
		$app = strtoupper(bin2hex(random_bytes(5)));
		// Generate secret key
		$secret = bin2hex(random_bytes(40));

		$reponse = $bdd->access()->prepare('SELECT * FROM users WHERE mail=?');
		$one = $login;
		$reponse->bindParam(1,$one);

		$date = date_create();
		$date = date_timestamp_get($date);

		$ip = $_SERVER['REMOTE_ADDR'];

		$reponse = $bdd->access()->prepare('INSERT INTO access (app,secret,time,ip,team_id) VALUES(?,?,?,?,?)');
		$reponse->execute(array($app,$secret,$date,$ip,$r->id));

		// And return infos
		$return =
		[
			'app' => $app,
			'secret' => $secret,
			'timestamp' => time(),
			'team_id' => $r->id
		];

		return $return;
	}
}
