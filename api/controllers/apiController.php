<?php

/*
Main controller
*/

class apiController{

    //public function authorize($method){

    public function authorize(){

        // get vars from headers
        if(!$app) $app = $_SERVER["HTTP_X_APPLICATION"];
        if(!$hash) $hash = $_SERVER["HTTP_X_SIGNATURE"];
        if(!$timestamp) $timestamp = $_SERVER["HTTP_X_TIMESTAMP"];

        // retrieve local secret with app uuid
        // without expiration:
		$bdd = new BDD(); // Get uuid();
		$reponse = $bdd->access()->prepare('SELECT * FROM access WHERE app=?');
		$one = $app;
		$reponse->bindParam(1,$one);
		$reponse->execute();
		//$compte = $reponse->fetch();
		$result = $reponse->fetchAll(PDO::FETCH_ASSOC);
		$r = json_decode(json_encode($result));

        // if result
        if($r){

            if(isset($_SERVER['HTTPS'])){
                $this->serverhttps = $_SERVER['HTTPS'];
            }else{
                $this->serverhttps = '';
            }

            $myhash = "$1$".sha1(
                    $_SERVER["REQUEST_METHOD"]
                    ."+".($this->serverhttps=='on'?"https://":"http://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]
                    ."+".file_get_contents("php://input")
                    ."+".$r->secret
                    ."+".$timestamp
            );

            if(!hash_equals($myhash,$hash)){

                $myhash = "$1$".sha1(
                        $_SERVER["REQUEST_METHOD"]
                        ."+".($_SERVER["HTTPS"]=='on'?"https://":"http://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]
                        ."+".''
                        ."+".$r->secret
                        ."+".$timestamp
                );
            }

            // check hashes equality
            if(hash_equals($myhash,$hash)){

                // retrieve user session
                $this->session = $r->session;
                $this->appId = $app;

                // Check if the user can access this route
                // @todo!!

                return true;
            }
            else return false;
        }
        else return false;

    }

}
