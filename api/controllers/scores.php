<?php
class scoring extends apiController{

	// /**
	// * @url GET /admin/scores/$id_game
	// * @noAuth
	// */
	// public function scores($id_game)
	// {
	// 	$bdd = new BDD();
    //
	// 	$reponse = $bdd->access()->prepare('SELECT id as id_team, name,begin,end,end_type FROM team WHERE end IS NOT NULL AND end_type IS NOT NULL AND id_game = "'.$id_game.'"');
	// 	$reponse->execute();
	// 	$scores = $reponse->fetchAll(PDO::FETCH_ASSOC);
    //
	// 	foreach ($scores as $key => $score) {
	// 		$temp = $scores[$key]['end'] - $scores[$key]['begin'];
    //
	// 		$scores[$key]['total_time'] = $temp;
	// 		$scores[$key]['time'] =  gmdate("H:i:s", $temp);;
    //
	// 		$reponse = $bdd->access()->prepare('SELECT * FROM team_hint WHERE id_team = "'.$score['id_team'].'"');
	// 		$reponse->execute();
	// 		$return = $reponse->fetchAll(PDO::FETCH_ASSOC);
	// 		foreach ($return as $key2 => $value) {
	// 		$scores[$key]['penality'] += intval($value['penality']);
	// 		}
	// 		$scores[$key]['total_time'] += $scores[$key]['penality'];
	// 		$scores[$key]['total_time'] = gmdate("H:i:s", $scores[$key]['total_time']);
    //
	// 		if ($scores[$key]['penality'] > 0) {
	// 			$scores[$key]['penality'] = gmdate("H:i:s", $scores[$key]['penality']);
	// 				$scores[$key]['state'] = 1;
	// 		}else {
	// 			$scores[$key]['penality'] = gmdate("H:i:s", $scores[$key]['penality']);
	// 				$scores[$key]['state'] = 0;
	// 		}
    //
	// 	}
    //
	// 	foreach ($scores as $key => $value) {
	// 		$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_team = "'.$value['id_team'].'"');
	// 		$reponse->execute();
	// 		$tkt = $reponse->fetchAll(PDO::FETCH_ASSOC);
    //
	// 		foreach ($tkt as $key2 => $value2) {
	// 			if ($value2['trouble']) {
	// 				$scores[$key]['trouble'] = $value2['trouble'];
	// 			}
	// 		}
	// 		$scores[$key]['questions'] = $this->questionScores($value['id_team']);
	// 	}
    //
	// 	return $scores;
    //
	// }

	/**
	* @url GET /admin/scores/question/$id_team
	* @noAuth
	*/
	public function questionScores($id_team)
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id as id_game FROM game WHERE active =1');
		$reponse->execute();
		$id_game = $reponse->fetch(PDO::FETCH_ASSOC);
		$id_game = $id_game['id_game'];

		$reponse = $bdd->access()->prepare('SELECT id as id_question, title FROM question WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);


		foreach ($questions as $key => $question) {

			//
			$reponse = $bdd->access()->prepare('SELECT * FROM hint WHERE id_question = "'.$question['id_question'].'"');
			$reponse->execute();
			$hints = $reponse->fetchAll(PDO::FETCH_ASSOC);
			//
			$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_question = "'.$question['id_question'].'" AND id_team = "'.$id_team.'"');
			$reponse->execute();
			$answer = $reponse->fetch(PDO::FETCH_ASSOC);

			if ($answer) {
				if ($answer['end'] === null) {
					$questions[$key]['message'] = 'Not Ended';
				}elseif ($answer['trouble'] == 1) {
					$time = $answer['end'] - $answer['begin'];
					$questions[$key]['message'] = 'Trouble';
					$questions[$key]['time'] = gmdate("H:i:s", $time);
				}else {
					$time = $answer['end'] - $answer['begin'];
					$questions[$key]['time'] = gmdate("H:i:s", $time);
				}
			}else {
				$questions[$key]['message'] = 'Not Begin';
			}

			$questions[$key]['penality'] =  gmdate("H:i:s", 0);
			$questions[$key]['state'] = 0;
			//
			foreach ($hints as $key2 => $value2) {
				$reponse = $bdd->access()->prepare('SELECT * FROM team_hint WHERE id_hint = "'.$value2['id'].'" AND id_team = "'.$id_team.'"');
				$reponse->execute();
				$penality = $reponse->fetch(PDO::FETCH_ASSOC);

				if ($penality) {
					$questions[$key]['penality'] = gmdate("H:i:s", $penality['penality']);
					$questions[$key]['state'] = 1;
				}
			}
		}
		return $questions;
	}


	/**
	* @url GET /admin/scores/$id_game
	* @noAuth
	*/
	public function test($id_game)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id as id_team,id_game,end_type,begin,end,name FROM team WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$teams = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($teams as $key => $team) {
			$teams[$key]['time_question'] = $team['end'] - $team['begin'];
			unset($teams[$key]['end']);
			unset($teams[$key]['begin']);
			unset($teams[$key]['id_game']);

			if ($teams[$key]['time_question'] < 0) {
				unset($teams[$key]['time_question']);
			}

			$teams[$key]['time_menu'] = $teams[$key]['time_question'];
			$teams[$key]['time_total'] = 0;
			$teams[$key]['penality'] = 0;
			$teams[$key]['trouble'] = 0;
			$teams[$key]['penality_state'] = 0;


			$reponse = $bdd->access()->prepare('SELECT id as id_question,title FROM question WHERE id_game = "'.$team['id_game'].'"');
			$reponse->execute();
			$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);
			$teams[$key]['game_questions'] = $questions;

			foreach ($teams[$key]['game_questions'] as $key2 => $value) {

				$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_question = "'.$value['id_question'].'" AND id_team = "'.$team['id_team'].'"');
				$reponse->execute();
				$teamQuestion = $reponse->fetch(PDO::FETCH_ASSOC);
				$teams[$key]['game_questions'][$key2]['team_question'] = $teamQuestion;

				$teams[$key]['game_questions'][$key2]['team_question']['time'] = $teams[$key]['game_questions'][$key2]['team_question']['end'] - $teams[$key]['game_questions'][$key2]['team_question']['begin'];

				unset($teams[$key]['game_questions'][$key2]['team_question']['id_question']);

				if ($teams[$key]['game_questions'][$key2]['team_question']['end'] == null && $teams[$key]['game_questions'][$key2]['team_question']['begin'] == null) {
					$teams[$key]['game_questions'][$key2]['team_question']['status'] = 'not begined';
				}elseif ($teams[$key]['game_questions'][$key2]['team_question']['end'] == null && $teams[$key]['game_questions'][$key2]['team_question']['begin'] != null) {
					$teams[$key]['game_questions'][$key2]['team_question']['status'] = 'not ended';
				}

				if ($teams[$key]['game_questions'][$key2]['team_question']['time'] <= 0) {
					unset($teams[$key]['game_questions'][$key2]['team_question']['time']);
				}

				unset($teams[$key]['game_questions'][$key2]['team_question']['begin']);
				unset($teams[$key]['game_questions'][$key2]['team_question']['end']);


				$teams[$key]['time_menu'] = $teams[$key]['time_menu'] - $teams[$key]['game_questions'][$key2]['team_question']['time'];

				if ($teams[$key]['game_questions'][$key2]['team_question']['trouble'] == 1) {
						$teams[$key]['trouble'] = 1;
				}

				$reponse = $bdd->access()->prepare('SELECT id as id_hint,id_question FROM hint WHERE id_question = "'.$value['id_question'].'"');
				$reponse->execute();
				$hints = $reponse->fetchAll(PDO::FETCH_ASSOC);

				foreach ($hints as $key3 => $hint) {

					$reponse = $bdd->access()->prepare('SELECT * FROM team_hint WHERE id_hint = "'.$hint['id_hint'].'" AND id_team = "'.$teams[$key]['game_questions'][$key2]['team_question']['id_team'].'"');
					$reponse->execute();
					$teamHint = $reponse->fetch(PDO::FETCH_ASSOC);

					if ($teamHint) {
						$teams[$key]['game_questions'][$key2]['team_question']['penality'] = gmdate("H:i:s", $teamHint['penality']);
					}

					$teams[$key]['penality'] = $teams[$key]['penality'] + $teamHint['penality'];

					if ($teams[$key]['penality'] > 0) {
						$teams[$key]['penality_state'] = 1;
					}

				}

				unset($teams[$key]['game_questions'][$key2]['team_question']['id_team']);

				$teams[$key]['game_questions'][$key2]['team_question']['time'] = gmdate("H:i:s", $teams[$key]['game_questions'][$key2]['team_question']['time']);
			}
			$teams[$key]['time_question'] = $teams[$key]['time_question'] - $teams[$key]['time_menu'];
			$teams[$key]['time_total'] = $teams[$key]['time_question'] + $teams[$key]['time_menu'] + $teams[$key]['penality'];
			$teams[$key]['time_total'] = gmdate("H:i:s", $teams[$key]['time_total']);
			$teams[$key]['time_question'] = gmdate("H:i:s", $teams[$key]['time_question']);
			$teams[$key]['time_menu'] = gmdate("H:i:s", $teams[$key]['time_menu']);
			$teams[$key]['penality'] = gmdate("H:i:s", $teams[$key]['penality']);
		}

		return $teams;
	}
}
