
<?php

class back extends apiController{

	/**
	* @url GET /back/$code
	* @noAuth
	*/
	public function getBack($code)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM team WHERE end IS NULL AND code = "'.$code.'"');
		$reponse->execute();
		$team = $reponse->fetch(PDO::FETCH_ASSOC);

		if ($team == false) {
			$reponse = $bdd->access()->prepare('SELECT end_type FROM team WHERE code = "'.$code.'"');
			$reponse->execute();
			$team = $reponse->fetch(PDO::FETCH_ASSOC);
			$return = new stdClass();
			$return->status = false;
			$return->raison = $team['end_type'];
			return $return;
		}else {
			$reponse = $bdd->access()->prepare('SELECT id_question,end FROM team_question_times WHERE id_team = "'.$team['id'].'"');
			$reponse->execute();
			$andswer = $reponse->fetchAll(PDO::FETCH_ASSOC);
			foreach ($andswer as $key => $value) {
				$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id = "'.$value['id_question'].'"');
				$reponse->execute();
				$questionReponse = $reponse->fetch(PDO::FETCH_ASSOC);
				if ($value['end']) {
					$team['answers'][$value['id_question']] = $questionReponse['reponse'];
				}
			}
			$reponse = $bdd->access()->prepare('SELECT * FROM team_hint WHERE id_team = "'.$team['id'].'"');
			$reponse->execute();
			$questionHints = $reponse->fetchAll(PDO::FETCH_ASSOC);

			foreach ($questionHints as $key => $value) {
				$reponse = $bdd->access()->prepare('SELECT * FROM hint WHERE id = "'.$value['id_hint'].'"');
				$reponse->execute();
				$hintQuestion = $reponse->fetch(PDO::FETCH_ASSOC);
				$team['hint'][$hintQuestion['id_question']][$value['id_hint']] = $hintQuestion['hint'];
			}
		}
		return $team;

	}
}
