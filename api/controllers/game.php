<?php
class first extends apiController{

	/**
	* @url GET /game
	* @url GET /game/$id_game
	* @url GET /game/$id_game/$id_team
	* @noAuth
	*/
	public function game($id_game = 0,$id_team = 0)
	{
		$bdd = new BDD();

		if ($id_game == 0 && $id_team == 0) {
			$reponse = $bdd->access()->prepare('SELECT id,title,time,intro,rules,default_game FROM game WHERE active =1');
			$reponse->execute();
			$games = $reponse->fetchAll(PDO::FETCH_ASSOC);

			if (empty($games)) {
				return 'no game';
			}else {
				foreach ($games as $key => $game) {
					$games[$key]['time'] = $game['time'] * 60;
				}
			}

			return $games;

		}elseif ($id_team !=0) {
			$temp = false;
			$reponse = $bdd->access()->prepare('SELECT id,title,addiction FROM question WHERE id_game = "'.$id_game.'"');
			$reponse->execute();
			$questionList = $reponse->fetchAll(PDO::FETCH_ASSOC);

			foreach ($questionList as $index =>$question) {

				$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_team = "'.$id_team.'" AND id_question = "'.$question['id'].'"');
				$reponse->execute();
				$entered = $reponse->fetch(PDO::FETCH_ASSOC);

				if (!$entered) {
					$questionList[$index]['entered'] = false;
				}elseif ($entered['end'] != NULL) {
					$questionList[$index]['ended'] = true;
				}else {
					$questionList[$index]['entered'] = true;
				}

				$reponse = $bdd->access()->prepare('SELECT id_game FROM question WHERE id = "'.$question['id'].'"');
				$reponse->execute();
				$game = $reponse->fetch(PDO::FETCH_ASSOC);

				$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$game['id_game'].'" ORDER BY "id"');
				$reponse->execute();
				$idFirstQuestion = $reponse->fetch(PDO::FETCH_ASSOC);

				if ($idFirstQuestion['id'] == $question['id']) {
						$questionList[$index]['enabled'] = true;
				}else {
						$questionList[$index]['enabled'] = false;
				}


				if ($question['addiction'] == 1) {

					$reponse = $bdd->access()->prepare('SELECT * FROM addiction WHERE id_question = "'.$question['id'].'"');
					$reponse->execute();
					$addictionList = $reponse->fetchAll(PDO::FETCH_ASSOC);
					$questionList[$index]['addictionQuestions'] = [];


					foreach ($addictionList as $indexAD => $addiction) {

						array_push(	$questionList[$index]['addictionQuestions'],$addiction['addiction_id_question']);

						$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_team = "'.$id_team.'" AND id_question = "'.$addiction['addiction_id_question'].'"');
						$reponse->execute();
						$addictionSuccess = $reponse->fetch(PDO::FETCH_ASSOC);
						if ($addictionSuccess['end'] != NULL) {
							$temp = false;
						}else {
							$temp = true;
						}

					}
					if ($temp == false) {
							$questionList[$index]['enabled'] = true;
					}else {
							$questionList[$index]['enabled'] = false;
					}
				}else {
						$questionList[$index]['enabled'] = true;
				}
			}

			return $questionList;
		}else {
			$reponse = $bdd->access()->prepare('SELECT id,title,time,intro,rules,menu FROM game WHERE id ="'.$id_game.'"');
			$reponse->execute();
			$return = $reponse->fetch(PDO::FETCH_ASSOC);
			$return['time'] = $return['time'] * 60;
			return $return;
		}
	}

	/**
	* @url POST /start
	* @noAuth
	*/
	public function start($data)
	{
		$data->teamName = addslashes($data->teamName);
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id,name,code FROM team WHERE name = "'.$data->teamName.'" AND id_game ="'.$data->game->id.'"');
		$reponse->execute();
		$teams = $reponse->fetch(PDO::FETCH_ASSOC);

		if (empty($teams)) {
			$now = new DateTime();
			$now->format('Y-m-d H:i:s');
			$date = $now->getTimestamp();
			$code = new random();
			$code = $code->string();
			$reponse = $bdd->access()->prepare('INSERT INTO team(name,id_game,begin,code) VALUES ("'.$data->teamName.'","'.$data->game->id.'","'.$date.'","'.$code.'")');
			$reponse->execute();

			$reponse = $bdd->access()->prepare('SELECT id,name,code FROM team WHERE name = "'.$data->teamName.'" AND id_game ="'.$data->game->id.'"');
			$reponse->execute();
			$teams = $reponse->fetch(PDO::FETCH_ASSOC);

			return $teams;
		}else{
			throw new Jacwright\RestServer\RestException(409,'Team name already used');
		}

	}

	/**
	* @url GET /check/time/$id_game/$id_team
	* @noAuth
	*/
	public function checkTime($id_game,$id_team)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM team WHERE id = "'.$id_team.'" AND id_game = "'.$id_game.'"');
		$reponse->execute();
		$beginTime = $reponse->fetch(PDO::FETCH_ASSOC);
		if ($beginTime['end'] != null) {
			$return['active'] = false;
			$return['left'] = false;
			return $return;
		}
		$beginTime = intval($beginTime['begin']);
		$intervalTime = $this->game($id_game);
		$intervalTime = intval($intervalTime['time']);
		$endTime = $beginTime + $intervalTime;
		$nowTime = new DateTime();
		$nowTime->format('Y-m-d H:i:s');
		$nowTime = $nowTime->getTimestamp();
		$leftTime = $endTime - $nowTime;
		if ($leftTime <= 0) {
			$return['active'] = false;
			$return['left'] = $leftTime;
			return $return;
		}else {
			$return['active'] = true;
			$return['left'] = $leftTime;
			return $return;
		}

	}


	/**
	* @url PATCH /stop/$id_team/$forced
	* @url PATCH /stop/$id_team
	* @noAuth
	*/
	public function endGame($id_team,$forced = false)
	{

			$now = new DateTime();
			$now->format('Y-m-d H:i:s');
			$date = $now->getTimestamp();
			$bdd = new BDD();

			$reponse = $bdd->access()->prepare('SELECT * FROM team WHERE id = "'.$id_team.'"');
			$reponse->execute();
			$team = $reponse->fetch(PDO::FETCH_ASSOC);

			$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$team['id_game'].'"');
			$reponse->execute();
			$game = $reponse->fetch(PDO::FETCH_ASSOC);


			if ($team['end_type'] == null) {
				if ($forced == 'true') {
					$reponse = $bdd->access()->prepare('UPDATE team SET end = "'.$date.'", end_type = "aborted" WHERE id = "'.$id_team.'"');
					$reponse->execute();
					$return = (object) array('aborted' => $game['aborted']);
					return $return;
				}else {
					$reponse = $bdd->access()->prepare('UPDATE team SET end = "'.$date.'", end_type = "time out" WHERE id = "'.$id_team.'"');
					$reponse->execute();
					$return = (object) array('aborted' => $game['timeout']);
					return $return;
				}

			}

	}

	/**
	* @url GET /fade/$id_game
	* @noAuth
	*/
	public function fades($id_game)
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM thanks WHERE id_game IS NULL');
		$reponse->execute();
		$return = $reponse->fetchAll(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT * FROM thanks WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$games = $reponse->fetchAll(PDO::FETCH_ASSOC);
		foreach ($games as $key => $value) {
			array_push($return,$value);
		}
		shuffle($return);
		return $return;
	}

}
