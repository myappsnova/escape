<?php

class interogations extends apiController{

	/**
	* @url GET /question/$id_question
	* @noAuth
	*/
	public function question($id_question)
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id,title,question,file,hint,addiction,type,text FROM question WHERE id = "'.$id_question.'"');
		$reponse->execute();
		$return = $reponse->fetch(PDO::FETCH_ASSOC);
		return $return;
	}

	/**
	* @url POST /tqt/$id_team/$id_question
	* @noAuth
	*/
	public function registerTQT($id_team,$id_question)
	{
			$now = new DateTime();
			$now->format('Y-m-d H:i:s');
			$date = $now->getTimestamp();

			$bdd = new BDD();

			$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_question = "'.$id_question.'" AND id_team = "'.$id_team.'"' );
			$reponse->execute();
			$return = $reponse->fetchAll(PDO::FETCH_ASSOC);

			if (count($return) == 0) {
				$reponse = $bdd->access()->prepare('INSERT INTO team_question_times(id_team,id_question,begin) VALUES ("'.$id_team.'","'.$id_question.'","'.$date.'")');
				$reponse->execute();
				throw new Jacwright\RestServer\RestException(201);
			}

	}


	/**
	* @url GET hint/question/$id_question/$id_team
	* @noAuth
	*/
	public function getQuestionHints($id_question,$id_team)
	{
		$now = new DateTime();
		$now->format('Y-m-d H:i:s');
		$date = $now->getTimestamp();

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM hint WHERE id_question = "'.$id_question.'" ORDER BY time');
		$reponse->execute();
		$hints = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($hints as $key => $hint) {

			$reponse = $bdd->access()->prepare('SELECT begin,end FROM team_question_times WHERE id_team = "'.$id_team.'" AND id_question = "'.$id_question.'"');
			$reponse->execute();
			$questionInfo = $reponse->fetch(PDO::FETCH_ASSOC);

			if ($questionInfo['begin'] && !$questionInfo['end']) {
				$hintTimer = ($hint['time'] * 60);
				$showHint = intval($questionInfo['begin']) + intval($hintTimer);
				if ($showHint >= $date) {
					unset($hints[$key]['hint']);
				}else{
					$hints[$key]['hint'] = true;
				}
			}elseif ($questionInfo['end']) {
					$hints[$key]['ended'] = true;
			}else {
				unset($hints[$key]['hint']);
			}

			$hints[$key]['timeleft'] =  ($questionInfo['begin'] + $hint['time'] * 60)- $date;
		}

		return $hints;
	}


	/**
	* @url GET hint/give/$id_hint/$id_team
	* @noAuth
	*/
	public function giveHint($id_hint,$id_team)
	{
		$now = new DateTime();
		$now->format('Y-m-d H:i:s');
		$date = $now->getTimestamp();

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id,id_question,time,hint FROM hint WHERE id = "'.$id_hint.'"');
		$reponse->execute();
		$hint = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT * FROM team_hint WHERE id_hint = "'.$id_hint.'" AND id_team = "'.$id_team.'"' );
		$reponse->execute();
		$teamHint = $reponse->fetchAll(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT begin FROM team_question_times WHERE id_team = "'.$id_team.'" AND id_question = "'.$hint['id_question'].'"');
		$reponse->execute();
		$beginTime = $reponse->fetch(PDO::FETCH_ASSOC);

		if ($beginTime['begin']) {
			$penality = intval($date) - intval($beginTime['begin']);
			$penality = $hint['time']*60 - $penality;
			$time = $hint['time']*60;

			if (count($teamHint) == 0) {
				if ($penality <= 0) {
					$reponse = $bdd->access()->prepare('INSERT INTO team_hint(id_team,id_hint,time,penality) VALUES ("'.intval($id_team).'","'.intval($id_hint).'","'.intval($date).'","0")');
					$reponse->execute();
				}else{
					$reponse = $bdd->access()->prepare('INSERT INTO team_hint(id_team,id_hint,time,penality) VALUES ("'.intval($id_team).'","'.intval($id_hint).'","'.intval($date).'","'.intval($penality).'")');
					$reponse->execute();
				}
				return $hint;
			}

		}
	}

	/**
	* @url POST question/answer/$id_team/$id_question
	* @noAuth
	*/
	public function answer($id_team,$id_question,$data)
	{

		$team_reponse = addslashes($data->reponse);
		$team_reponse = strtolower($team_reponse);

		$now = new DateTime();
		$now->format('Y-m-d H:i:s');
		$date = $now->getTimestamp();

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id,reponse,id_game,nearby,trouble FROM question WHERE id = "'.$id_question.'"');
		$reponse->execute();
		$questionReponse = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$questionReponse['id_game'].'"');
		$reponse->execute();
		$game = $reponse->fetch(PDO::FETCH_ASSOC);

		//trouble
		if ($questionReponse['trouble'] == $team_reponse) {


			$reponse = $bdd->access()->prepare('UPDATE team_question_times SET end = "'.$date.'",trouble = "1" WHERE id_question = "'.$id_question.'" AND id_team = "'.$id_team.'"');
			$reponse->execute();

			$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$questionReponse['id_game'].'"');
			$reponse->execute();
			$totalQuestions = $reponse->fetchAll(PDO::FETCH_ASSOC);

			$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_team = "'.$id_team.'"');
			$reponse->execute();
			$totalAnswers = $reponse->fetchAll(PDO::FETCH_ASSOC);

			if (count($totalQuestions) == count($totalAnswers)) {

				$end = true;

				foreach ($totalAnswers as $key => $answer) {
					if ($answer['end'] === null) {
						$end = false;
					}
				}
				if ($end == false) {
					$goodAnswer = (object)['good' => true, 'answer' => $team_reponse, 'id_question' => $id_question];
					return $goodAnswer;
				}else {
					$reponse = $bdd->access()->prepare('UPDATE team SET end = "'.$date.'", end_type = "success" WHERE id = "'.$id_team.'"');
					$reponse->execute();
					$return = (object) ['success' => $game['conclusion']];
					return $return;
				}
			}else {
				$goodAnswer = (object)['good' => true, 'answer' => $team_reponse, 'id_question' => $id_question];
				return $goodAnswer;
			}

		}


		if ($questionReponse['reponse'] == $team_reponse) {
			//save information
			$reponse = $bdd->access()->prepare('UPDATE team_question_times SET end = "'.$date.'" WHERE id_question = "'.$id_question.'" AND id_team = "'.$id_team.'"');
			$reponse->execute();

			$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$questionReponse['id_game'].'"');
			$reponse->execute();
			$totalQuestions = $reponse->fetchAll(PDO::FETCH_ASSOC);

			$reponse = $bdd->access()->prepare('SELECT * FROM team_question_times WHERE id_team = "'.$id_team.'"');
			$reponse->execute();
			$totalAnswers = $reponse->fetchAll(PDO::FETCH_ASSOC);

			if (count($totalQuestions) == count($totalAnswers)) {

				$end = true;

				foreach ($totalAnswers as $key => $answer) {
					if ($answer['end'] === null) {
						$end = false;
					}
				}
				if ($end == false) {
					$goodAnswer = (object)['good' => true, 'answer' => $team_reponse, 'id_question' => $id_question];
					return $goodAnswer;
				}else {
					$reponse = $bdd->access()->prepare('UPDATE team SET end = "'.$date.'", end_type = "success" WHERE id = "'.$id_team.'"');
					$reponse->execute();
					$return = (object) ['success' => $game['conclusion']];
					return $return;
				}
			}else {
				$goodAnswer = (object)['good' => true, 'answer' => $team_reponse, 'id_question' => $id_question];
				return $goodAnswer;
			}

		}elseif ($questionReponse['nearby'] == 1) {

			$reponse = $bdd->access()->prepare('SELECT * FROM nearby WHERE id_question = "'.$id_question.'"');
			$reponse->execute();
			$nearbys = $reponse->fetchAll(PDO::FETCH_ASSOC);

			foreach ($nearbys as $key => $value) {
				if ($value['reponse'] == $team_reponse) {
					$return =  (object)['reponse' => $value['near_answer']];
				return $return;
				}
			}

			$reponse = $bdd->access()->prepare('SELECT reponse FROM bad_answers ORDER BY RAND()');
			$reponse->execute();
			$badAnswer = $reponse->fetchAll(PDO::FETCH_ASSOC);
			return $badAnswer[0];


		}else {
			$reponse = $bdd->access()->prepare('SELECT reponse FROM bad_answers ORDER BY RAND()');
			$reponse->execute();
			$badAnswer = $reponse->fetchAll(PDO::FETCH_ASSOC);
			return $badAnswer[0];
		}
	}
}
