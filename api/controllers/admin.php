<?php
class powers extends apiController{

	/**
	* @url GET /admin/administration
	* @noAuth
	*/
	public function see()
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM admin');
		$reponse->execute();
		$administration = $reponse->fetch(PDO::FETCH_ASSOC);

		if ($administration == false) {
			return 'register';
		}else{
			return 'login';
		}
	}

	/**
	* @url POST /admin/create
	* @noAuth
	*/
	public function create($data)
	{
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}

		$hash = new hashMyPass(); // Hash pass
		$password = $hash->get($data->password);

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('INSERT INTO admin(mail,password) VALUES ("'.$data->mail.'","'.$password.'")');
		$reponse->execute();
	}

	/**
	* @url POST /admin/login
	* @noAuth
	*/
	public function login($data)
	{

		$bdd = new BDD();

		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}

		$reponse = $bdd->access()->prepare('SELECT * FROM admin WHERE mail = "'.$data->mail.'"');
		$reponse->execute();
		$login = $reponse->fetch(PDO::FETCH_ASSOC);

		if (!$login) {
	   	 throw new Jacwright\RestServer\RestException(406, 'Incorrect Login');
		 }elseif (!hash_equals($login['password'], crypt($data->password, $login['password']))) {
 			// check old version
 			if (sha1($data->password) != $login['password']) {
 				throw new Jacwright\RestServer\RestException(406, 'Incorrect Password');
 			}else {
 				return true;
 			}
 		}
	}

	/**
	* @url GET /admin/info
	* @noAuth
	*/
	public function info()
	{
		$now = new DateTime();
		$now->format('Y-m-d H:i:s');
		$date = $now->getTimestamp();

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE active = 1');
		$reponse->execute();
		$info = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT count(id_game)AS played FROM team WHERE end IS NOT NULL AND id_game ="'.$info['id'].'"');
		$reponse->execute();
		$played = $reponse->fetch(PDO::FETCH_ASSOC);
		$info['played'] = $played['played'];

		$reponse = $bdd->access()->prepare('SELECT count(id_game)AS aborted FROM team WHERE end_type = "aborted" AND id_game ="'.$info['id'].'"');
		$reponse->execute();
		$aborted = $reponse->fetch(PDO::FETCH_ASSOC);
		$info['aborted'] = $aborted['aborted'];

		$reponse = $bdd->access()->prepare('SELECT count(id_game)AS success FROM team WHERE end_type = "success" AND id_game ="'.$info['id'].'"');
		$reponse->execute();
		$success = $reponse->fetch(PDO::FETCH_ASSOC);
		$info['success'] = $success['success'];

		$reponse = $bdd->access()->prepare('SELECT count(id_game)AS timeout FROM team WHERE end_type = "time out" AND id_game ="'.$info['id'].'"');
		$reponse->execute();
		$timeout = $reponse->fetch(PDO::FETCH_ASSOC);
		$info['timeout'] = $timeout['timeout'];

		$reponse = $bdd->access()->prepare('SELECT * FROM team WHERE id_game ="'.$info['id'].'" AND end IS NULL');
		$reponse->execute();
		$teams = $reponse->fetchAll(PDO::FETCH_ASSOC);
		$info['playing'] = 0;
		$info['unknow'] = 0;

		foreach ($teams as $key => $team) {
			$gameTime = $info['time']*60;
			$gameTime = $gameTime + $team['begin'];
			//return $gameTime .'  /  '. $date;
			if ($gameTime > $date) {
				$info['playing'] ++;
			}else {
				$info['unknow'] ++;
			}
		}
		return $info;
	}

	/**
	* @url GET /admin/games
	* @noAuth
	*/
	public function getGames()
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM game');
		$reponse->execute();
		$info = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $info;
	}

	/**
	* @url POST /admin/game
	* @noAuth
	*/
	public function postGame($data)
	{
		$bdd = new BDD();
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}
		$reponse = $bdd->access()->prepare('INSERT INTO game (title,time,intro,menu,conclusion,timeout,aborted,rules) VALUES ("'.$data->title.'","'.$data->time.'","'.$data->intro.'","'.$data->menu.'","'.$data->conclusion.'","'.$data->timeout.'","'.$data->aborted.'","'.$data->rules.'")');
		$reponse->execute();

	}

	/**
	* @url POST /admin/game/clone/$id_game
	* @noAuth
	*/
	public function cloneGame($id_game)
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$id_game.'"');
		$reponse->execute();
		$game = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('INSERT INTO game (title,time,intro,menu,conclusion,timeout,aborted,rules) VALUES ("'.$game['title'].'","'.$game['time'].'","'.$game['intro'].'","'.$game['menu'].'","'.$game['conclusion'].'","'.$game['timeout'].'","'.$game['aborted'].'","'.$game['rules'].'")');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('SELECT * FROM game ORDER BY id DESC');
		$reponse->execute();
		$idLastGame = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($questions as $key => $value) {
			$reponse = $bdd->access()->prepare('INSERT INTO question (id_game,title,question,reponse,nearby,hint,addiction,type,file,trouble) VALUES ("'.$idLastGame['id'].'","'.$value['title'].'","'.$value['question'].'","'.$value['reponse'].'","'.$value['nearby'].'","'.$value['hint'].'","'.$value['addiction'].'","'.$value['type'].'","'.$value['file'].'","'.$value['trouble'].'")');
			$reponse->execute();

			$reponse = $bdd->access()->prepare('SELECT * FROM neaby WHERE id_question = "'.$value['id_question'].'"');
			$reponse->execute();
			$nearbys = $reponse->fetchAll(PDO::FETCH_ASSOC);
		}
	}

	/**
	* @url DELETE /admin/game/$id_game
	* @noAuth
	*/
	public function deleteGame($id_game)
	{
		$bdd = new BDD();

		$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($questions as $key => $question) {
			$up = new UPL();
			$target_dir = $up->uploadDir();
			unlink($target_dir.$question['file']);
		}

		$reponse = $bdd->access()->prepare('DELETE FROM game WHERE id = "'.$id_game.'"');
		$reponse->execute();
	}

	/**
	* @url PATCH /admin/games/$state/$id_game
	* @noAuth
	*/
	public function activeDesactive($state,$id_game)
	{

		$bdd = new BDD();

		// // ********** if only one game at time ********** // //
		// $reponse = $bdd->access()->prepare('SELECT * FROM game WHERE active = "1"');
		// $reponse->execute();
		// $activeGame = $reponse->fetchAll(PDO::FETCH_ASSOC);
        //
		// foreach ($activeGame as $key => $game) {
		// 	if ($game['active'] == 1) {
		// 		$reponse = $bdd->access()->prepare('UPDATE game SET active = "0" WHERE id = "'.$game['id'].'"');
		// 		$reponse->execute();
		// 	}
		// }
		// // ********** if only one game at time ********** // //

		$reponse = $bdd->access()->prepare('UPDATE game SET active = "'.$state.'" WHERE id = "'.$id_game.'"');
		$reponse->execute();
	}

	/**
	* @url PATCH /admin/default/games/$state/$id_game
	* @noAuth
	*/
	public function activeDesactiveDefault($state,$id_game)
	{

		$bdd = new BDD();


		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE default_game = "1"');
		$reponse->execute();
		$activeGame = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($activeGame as $key => $game) {
			if ($game['default_game'] == 1) {
				$reponse = $bdd->access()->prepare('UPDATE game SET default_game = "0" WHERE id = "'.$game['id'].'"');
				$reponse->execute();
			}
		}


		$reponse = $bdd->access()->prepare('UPDATE game SET default_game = "'.$state.'" WHERE id = "'.$id_game.'"');
		$reponse->execute();
	}

	/**
	* @url GET /admin/game/$id_game
	* @noAuth
	*/
	public function getSelectedGame($id_game)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$id_game.'"');
		$reponse->execute();
		$game = $reponse->fetch(PDO::FETCH_ASSOC);
		return $game;
	}

	/**
	* @url GET /admin/list/games
	* @url GET /admin/list/games/$status
	* @url GET /admin/list/games/$status/$id_game
	* @noAuth
	*/
	public function getListGames($status = false, $id_game = false)
	{
		$bdd = new BDD();

		if (!$status) {
			$reponse = $bdd->access()->prepare('SELECT * FROM game');
			$reponse->execute();
			$games = $reponse->fetchAll(PDO::FETCH_ASSOC);
		}elseif ($id_game) {
			$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$id_game.'"');
			$reponse->execute();
			$games = $reponse->fetch(PDO::FETCH_ASSOC);
		}else {
			$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE active = 1');
			$reponse->execute();
			$games = $reponse->fetch(PDO::FETCH_ASSOC);

			if (!$games) {
				$reponse = $bdd->access()->prepare('SELECT * FROM game ORDER BY id DESC');
				$reponse->execute();
				$games = $reponse->fetch(PDO::FETCH_ASSOC);
			}
		}

		return $games;
	}

	/**
	* @url GET /admin/list/questions/$id_game/$id_question
	* @noAuth
	*/
	public function getListQuestions($id_game,$id_question)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT id,title FROM question WHERE id_game = "'.$id_game.'" AND id <> "'.$id_question.'"');
		$reponse->execute();
		$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $questions;
	}

	/**
	* @url PATCH /admin/game/$id_game/update
	* @noAuth
	*/
	public function updateGame($id_game,$data)
	{
		$bdd = new BDD();
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}
		$reponse = $bdd->access()->prepare('UPDATE game SET title = "'.$data->title.'", time = "'.$data->time.'", intro = "'.$data->intro.'", menu = "'.$data->menu.'", conclusion = "'.$data->conclusion.'", timeout = "'.$data->timeout.'", aborted = "'.$data->aborted.'", rules = "'.$data->rules.'" WHERE id = "'.$id_game.'"');
		$reponse->execute();
	}

	/**
	* @url GET /admin/questions/$id_game
	* @noAuth
	*/
	public function getSelectedGameQuestions($id_game)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$questions = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $questions;
	}

	/**
	* @url POST /admin/questions/$id_game
	* @noAuth
	*/
	public function postQuestions($id_game,$data)
	{
		$data->reponse = strtolower($data->reponse);
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('INSERT INTO question (id_game,title,question,reponse,nearby,hint,addiction,type,file,trouble,text) VALUES ("'.$id_game.'","'.$data->title.'","'.$data->question.'","'.$data->reponse.'","0","0","0","'.$data->type.'","'.$data->file.'","'.$data->trouble.'","'.$data->text.'")');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id_game = "'.$id_game.'" ORDER BY id DESC');
		$reponse->execute();
		$question = $reponse->fetch(PDO::FETCH_ASSOC);

		return $question;
	}


	/**
	* @url PATCH /admin/questions
	* @noAuth
	*/
	public function patchQuestion($data)
	{
		$bdd = new BDD();
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}
		$data->reponse = strtolower($data->reponse);
		$reponse = $bdd->access()->prepare('UPDATE question SET title = "'.$data->title.'", question = "'.$data->question.'", reponse = "'.$data->reponse.'", nearby = "'.$data->nearby.'", hint = "'.$data->hint.'", addiction = "'.$data->addiction.'", type = "'.$data->type.'", trouble = "'.$data->trouble.'", text = "'.$data->text.'" WHERE id = "'.$data->id.'"');
		$reponse->execute();
	}

	/**
	* @url DELETE /admin/questions/$id_question
	* @noAuth
	*/
	public function deleteQuestion($id_question)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM question WHERE id = "'.$id_question.'"');
		$reponse->execute();
		$question = $reponse->fetch(PDO::FETCH_ASSOC);

		$up = new UPL();
		$target_dir = $up->uploadDir();

		unlink($target_dir.$question['file']);

		$reponse = $bdd->access()->prepare('DELETE FROM question WHERE id = "'.$id_question.'"');
		$reponse->execute();
	}

	/**
	* @url GET /admin/hints/$id_question
	* @noAuth
	*/
	public function getHints($id_question)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM hint WHERE id_question = "'.$id_question.'"');
		$reponse->execute();
		$hints = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $hints;
	}

	/**
	* @url POST /admin/hint/$id_question
	* @noAuth
	*/
	public function postHint($id_question,$data)
	{
		$bdd = new BDD();
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}
		$reponse = $bdd->access()->prepare('INSERT INTO hint (id_question,hint,time) VALUES ("'.$id_question.'","'.$data->hint.'","'.$data->time.'")');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('UPDATE question SET hint = 1 WHERE id = "'.$id_question.'"');
		$reponse->execute();
	}

	/**
	* @url PATCH /admin/hints
	* @noAuth
	*/
	public function patchHint($data)
	{
		$data = $data->data;
		$bdd = new BDD();
		foreach ($data as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$data->$key->key2 = addslashes($value);
			}
		}
		foreach ($data as $key => $value) {
			$reponse = $bdd->access()->prepare('UPDATE hint SET hint = "'.$value->hint.'", time = "'.$value->time.'" WHERE id = "'.$value->id.'"');
			$reponse->execute();
		}
	}

	/**
	* @url DELETE /admin/hint/$id_hint
	* @noAuth
	*/
	public function deleteHint($id_hint)
	{

		$bdd = new BDD();

		$reponse = $bdd->access()->prepare('SELECT id_question FROM hint WHERE id = "'.$id_hint.'"');
		$reponse->execute();
		$question = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('DELETE FROM hint WHERE id = "'.$id_hint.'"');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('SELECT * FROM hint WHERE id_question = "'.$question['id_question'].'"');
		$reponse->execute();
		$hint = $reponse->fetch(PDO::FETCH_ASSOC);

		if (!$hint) {
			$reponse = $bdd->access()->prepare('UPDATE question SET hint = "0" WHERE id = "'.$question['id_question'].'"');
			$reponse->execute();
		}

	}

	/**
	* @url GET /admin/addictions/$id_question
	* @noAuth
	*/
	public function getAddictions($id_question)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM addiction WHERE id_question = "'.$id_question.'"');
		$reponse->execute();
		$addictions = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($addictions as $key => $value) {
			$reponse = $bdd->access()->prepare('SELECT title FROM question WHERE id = "'.$value['addiction_id_question'].'"');
			$reponse->execute();
			$title = $reponse->fetch(PDO::FETCH_ASSOC);
			$addictions[$key]['title'] = $title['title'];
		}
		return $addictions;
	}

	/**
	* @url POST /admin/addiction/$id_question
	* @noAuth
	*/
	public function postAddiction($id_question,$data)
	{
		$bdd = new BDD();

		$reponse = $bdd->access()->prepare('INSERT INTO addiction (id_question,addiction_id_question) VALUES ("'.$id_question.'","'.$data.'")');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('UPDATE question SET addiction = 1 WHERE id = "'.$id_question.'"');
		$reponse->execute();
	}

	/**
	* @url DELETE /admin/addiction/$id_addiction
	* @noAuth
	*/
	public function deleteAddiction($id_addiction)
	{

		$bdd = new BDD();

		$reponse = $bdd->access()->prepare('SELECT id_question FROM addiction WHERE id = "'.$id_addiction.'"');
		$reponse->execute();
		$question = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('DELETE FROM addiction WHERE id = "'.$id_addiction.'"');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('SELECT * FROM addiction WHERE id_question = "'.$question['id_question'].'"');
		$reponse->execute();
		$addiction = $reponse->fetch(PDO::FETCH_ASSOC);

		if (!$addiction) {
			$reponse = $bdd->access()->prepare('UPDATE question SET addiction = "0" WHERE id = "'.$question['id_question'].'"');
			$reponse->execute();
		}

	}

	/**
	* @url GET /admin/nearby/$id_question
	* @noAuth
	*/
	public function getNearbys($id_question)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM nearby WHERE id_question = "'.$id_question.'"');
		$reponse->execute();
		$nearby = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $nearby;
	}

	/**
	* @url POST /admin/nearby/$id_question
	* @noAuth
	*/
	public function postNearby($id_question,$data)
	{
		$bdd = new BDD();
		$data->reponse = strtolower($data->reponse);

		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}

		$reponse = $bdd->access()->prepare('INSERT INTO nearby (id_question,reponse,near_answer) VALUES ("'.$id_question.'","'.$data->reponse.'","'.$data->near_answer.'")');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('UPDATE question SET nearby = 1 WHERE id = "'.$id_question.'"');
		$reponse->execute();
	}

	/**
	* @url PATCH /admin/nearbys
	* @noAuth
	*/
	public function patchNearby($data)
	{
		$data = $data->data;
		$bdd = new BDD();

		foreach ($data as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$data[$key]->$key2 = strtolower($data[$key]->$key2);
				$data[$key]->$key2 = addslashes($data[$key]->$key2);
			}
		}
		foreach ($data as $key => $value) {
			$reponse = $bdd->access()->prepare('UPDATE nearby SET reponse = "'.$value->reponse.'",near_answer = "'.$value->near_answer.'" WHERE id = "'.$value->id.'"');
			$reponse->execute();
		}
	}

	/**
	* @url DELETE /admin/nearby/$id_nearby
	* @noAuth
	*/
	public function deleteNearby($id_nearby)
	{

		$bdd = new BDD();

		$reponse = $bdd->access()->prepare('SELECT id_question FROM nearby WHERE id = "'.$id_nearby.'"');
		$reponse->execute();
		$question = $reponse->fetch(PDO::FETCH_ASSOC);

		$reponse = $bdd->access()->prepare('DELETE FROM nearby WHERE id = "'.$id_nearby.'"');
		$reponse->execute();

		$reponse = $bdd->access()->prepare('SELECT * FROM nearby WHERE id_question = "'.$question['id_question'].'"');
		$reponse->execute();
		$nearby = $reponse->fetch(PDO::FETCH_ASSOC);

		if (!$nearby) {
			$reponse = $bdd->access()->prepare('UPDATE question SET nearby = "0" WHERE id = "'.$question['id_question'].'"');
			$reponse->execute();
		}
	}

	/**
	* @url DELETE /admin/teams/$id_game
	* @noAuth
	*/
	public function deleteTeams($id_game)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('DELETE FROM team WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
	}

	/**
	* @url DELETE /admin/ugly/teams/$id_game
	* @noAuth
	*/
	public function deleteUglyTeams($id_game)
	{
		$now = new DateTime();
		$now->format('Y-m-d H:i:s');
		$date = $now->getTimestamp();

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM game WHERE id = "'.$id_game.'"');
		$reponse->execute();
		$time = $reponse->fetch(PDO::FETCH_ASSOC);

		$time = $time['time'];
		$time = $time * 60;

		$reponse = $bdd->access()->prepare('SELECT * FROM team WHERE id_game = "'.$id_game.'"');
		$reponse->execute();
		$teams = $reponse->fetchAll(PDO::FETCH_ASSOC);

		foreach ($teams as $key => $team) {
			$begin = $team['begin'];
			$normalEnd = $begin + $time;

			if ($date > $normalEnd) {
				$reponse = $bdd->access()->prepare('DELETE FROM team WHERE id = "'.$team['id'].'" AND end IS NULL');
				$reponse->execute();
			}
		}
	}

	/**
	* @url GET /admin/application/thanks
	* @noAuth
	*/
	public function getThanks()
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM thanks');
		$reponse->execute();
		$thanks = $reponse->fetchAll(PDO::FETCH_ASSOC);
		foreach ($thanks as $key => $thx) {
			if ($thx['id_game'] == null) {
				$thanks[$key]['id_game'] = 'global';
			}
		}
		return $thanks;
	}

	/**
	* @url DELETE /admin/application/thanks/$id
	* @noAuth
	*/
	public function deleteThanks($id)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('DELETE FROM thanks WHERE id = "'.$id.'"');
		$reponse->execute();
	}

	/**
	* @url POST /admin/application/thanks
	* @noAuth
	*/
	public function postThanks($data)
	{
		$bdd = new BDD();

		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}

		if (!$data->id_game) {
			$reponse = $bdd->access()->prepare('INSERT INTO thanks (thx,id_game) VALUES ("'.$data->thx.'",NULL)');
			$reponse->execute();
		}else {
			$reponse = $bdd->access()->prepare('INSERT INTO thanks (thx,id_game) VALUES ("'.$data->thx.'","'.$data->id_game.'")');
			$reponse->execute();
		}
	}

	/**
	* @url PATCH /admin/application/thanks
	* @noAuth
	*/
	public function patchThanks($data)
	{
		$data = $data->data;
		$bdd = new BDD();

		foreach ($data as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$data[$key]->$key2 = addslashes($data[$key]->$key2);
			}
		}
		
		foreach ($data as $key => $value) {
			if ($value->id_game != "global") {
				$reponse = $bdd->access()->prepare('UPDATE thanks SET thx = "'.$value->thx.'", id_game = "'.$value->id_game.'"  WHERE id = "'.$value->id.'"');
				$reponse->execute();
			}else {
				$reponse = $bdd->access()->prepare('UPDATE thanks SET thx = "'.$value->thx.'", id_game = NULL  WHERE id = "'.$value->id.'"');
				$reponse->execute();
			}

		}
	}

	/**
	* @url GET /admin/application/bad_answers
	* @noAuth
	*/
	public function getbadAnswers()
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('SELECT * FROM bad_answers');
		$reponse->execute();
		$return = $reponse->fetchAll(PDO::FETCH_ASSOC);
		return $return;
	}

	/**
	* @url PATCH /admin/application/bad_answers
	* @noAuth
	*/
	public function patchBadAnswers($data)
	{
		$data = $data->data;
		$bdd = new BDD();

		foreach ($data as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$data[$key]->$key2 = addslashes($data[$key]->$key2);
			}
		}

		foreach ($data as $key => $value) {
			$reponse = $bdd->access()->prepare('UPDATE bad_answers SET reponse = "'.$value->reponse.'" WHERE id = "'.$value->id.'"');
			$reponse->execute();
		}
	}

	/**
	* @url POST /admin/application/bad_answers
	* @noAuth
	*/
	public function postBadAnswer($data)
	{
		foreach ($data as $key => $value) {
			$data->$key = addslashes($value);
		}

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('INSERT INTO bad_answers (reponse) VALUES ("'.$data->reponse.'")');
		$reponse->execute();

	}

	/**
	* @url DELETE /admin/application/bad_answers/$id
	* @noAuth
	*/
	public function deleteBadAnswers($id)
	{

		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('DELETE FROM bad_answers WHERE id = "'.$id.'"');
		$reponse->execute();
	}

	/**
	* @url DELETE /admin/tester
	* @noAuth
	*/
	public function deleteTester()
	{
		$bdd = new BDD();
		$reponse = $bdd->access()->prepare('DELETE FROM team WHERE name = "TESTER"');
		$reponse->execute();
	}

}
