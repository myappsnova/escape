<?php
 error_reporting(E_ALL & ~E_NOTICE);
 ini_set('display_errors', '1');

date_default_timezone_set("Europe/Paris");

// require_once 'config/config.php';
require_once 'classes/hashmypass.php';
require_once 'classes/sql.php';
require_once 'classes/upload.php';
require_once 'classes/random.php';

require_once 'libs/jacwright/RestServer.php';

$server = new \Jacwright\RestServer\RestServer('debug');
// $server->useCors = true;
// $server->allowedOrigin = 'http://example.com';
// // or use array of multiple origins
// $server->allowedOrigin = array('http://example.com', 'https://example.com');
// // or a wildcard
$server->allowedOrigin = '*';

// if($config->application->env == 'local'){
//     if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
//     }
// }

header('Access-Control-Allow-Headers: Authorization, content-type');

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}



require 'controllers/apiController.php';
$server->addClass('apiController');

require 'controllers/auth.php';
$server->addClass('auth');

require 'controllers/game.php';
$server->addClass('first');

require 'controllers/getBack.php';
$server->addClass('back');

require 'controllers/scores.php';
$server->addClass('scoring');

require 'controllers/questions.php';
$server->addClass('interogations');

require 'controllers/admin.php';
$server->addClass('powers');

require 'controllers/upload.php';
$server->addClass('files');

require 'controllers/version.php';
$server->addClass('vers');

$server->handle();
