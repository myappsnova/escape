<?php


class hashMyPass
{
    public function get($data){

        $cost = 10;
        // Create a random salt
        $salt = strtr(base64_encode(random_bytes(16)), '+', '.');
        // Prefix information about the hash so PHP knows how to verify it later.
        $salt = sprintf('$2a$%02d$', $cost).$salt;
        // Hash the password with the salt
        $hash = crypt($data, $salt);

        return $hash;
    }

}
