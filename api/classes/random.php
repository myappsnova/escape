<?php
class random
{
	//function string($length = 8, $alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	function string($length = 8, $alphabet = 'abcdefghjkmnpqrstuvwxyz123456789ABCDEFGHJKMNPQRSTUVWXYZ')
	{
	    if ($length < 1) {
	        throw new InvalidArgumentException('Length must be a positive integer');
	    }
	    $str = '';
	    $alphamax = strlen($alphabet) - 1;
	    if ($alphamax < 1) {
	        throw new InvalidArgumentException('Invalid alphabet');
	    }
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $alphabet[random_int(0, $alphamax)];
	    }
	    return $str;
	}
}
