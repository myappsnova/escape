app.controller('endCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams) {


	for(i=0; i<$rootScope.intervals.length; i++){
	    window.clearInterval($rootScope.intervals[i]);
	}

	if ($rootScope.ended) {
		if ('aborted' in $rootScope.ended) {
			//document.body.style = "background: url('images/timeout.jpg') no-repeat center center fixed; background-size: cover;";
			$scope.endMessage = $rootScope.ended['aborted'];
		}else if ('success' in $rootScope.ended) {
			document.body.style = "background: url('images/end.jpg') no-repeat center center fixed; background-size: cover;";
			$scope.endMessage = $rootScope.ended['success'];
		}
	}

	$scope.home = function(){
		$rootScope.reloadRoute();
	}



}]); // fin dont delete XD
