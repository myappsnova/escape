app.controller('questionCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval) {

	//if no teamname return home
	if ($rootScope.teamName == undefined) {
			$state.go('app.home');
	}

	$scope.image = false;
	$scope.text = false;
	$scope.sound = false;
	$scope.hints = [];
	$scope.hintButton = [];
	$scope.answers = $rootScope.goodAnswers;

	if ($rootScope.hintsShow == undefined) {
		$rootScope.hintsShow = [];
	}

	$http.get(config.getApiPath() + '/question/' + $rootScope.id_question).then(function(question) {
		$scope.data = question.data;
		$scope.type = function() {
				switch ($scope.data.type) {
					case '0':
						$scope.image = true;
						$scope.text = false;
						$scope.sound = false;
						break;
					case '1':
						$scope.image = false;
						$scope.text = false;
						$scope.sound = true;
						break;
					case '2':
						$scope.image = false;
						$scope.text = true;
						$scope.sound = false;
						break;
				}
		}
		$scope.type();

		if ($scope.data.hint > 0) {
			$http.get(config.getApiPath() + '/hint/question/'+$rootScope.id_question+'/'+$rootScope.team.id).then(function(hints) {
				$scope.hints = hints.data;

				for (var i = 0; i < hints.data.length; i++) {
					var distance = hints.data[i].timeleft * 1000;
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.hints[i]['timeleftFormated'] = hours + "h " + minutes + "m " + seconds + "s ";

					if ($scope.hints[i]['hint'] == true) {
						$scope.hintButton[i] = 'btn-info';
					}else {
						$scope.hintButton[i] = 'btn-danger';
					}
				}
			})
			var stop;
			$rootScope.timerHint = function() {
				if ( angular.isDefined(stop) ) return;
				stop = $interval(function() {
					$http.get(config.getApiPath() + '/hint/question/'+$rootScope.id_question+'/'+$rootScope.team.id).then(function(hints) {
						$scope.hints = hints.data;
						for (var i = 0; i < hints.data.length; i++) {

							var distance = hints.data[i].timeleft * 1000;
							var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
							var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);
							$scope.hints[i]['timeleftFormated'] = hours + "h " + minutes + "m " + seconds + "s ";

							if ($scope.hints[i]['hint'] == true) {
								$scope.hintButton[i] = 'btn-info';
							}else {
								$scope.hintButton[i] = 'btn-danger';
							}
						}
					})
				}, 1000);
			}
			$rootScope.timerHint();

			$rootScope.stopTimerHints = function() {
						if (angular.isDefined(stop)) {
							$interval.cancel(stop);
							stop = undefined;
						}
			};

		}
	});
	$scope.getHint = function(id_hint,index){
		$http.get(config.getApiPath() +'/hint/give/'+id_hint.id+'/'+ $rootScope.team.id).then(function(hint) {
			hint = hint.data;
			$rootScope.hintsShow[hint.id] = hint.hint;
		});
	}


	$scope.answer = function(reponse){
		$scope.response = reponse;
		$scope.badReponse ='';
		reponse = {'reponse':reponse};
		$http.post(config.getApiPath() +'/question/answer/'+$rootScope.team.id+ '/'+ $rootScope.id_question, reponse).then(function(reponseReturn) {
			if (reponseReturn.data.good == true) {
				$rootScope.goodAnswers[reponseReturn.data.id_question] = reponseReturn.data.answer;
				$state.go('app.menu')
			}else if ('success' in reponseReturn.data) {
					$rootScope.ended= {'success':reponseReturn.data['success']};
					delete $rootScope.questions;
					$rootScope.stopTimer();
					$rootScope.stopTimerHints();
					$state.go('app.end');
			}else {
				$scope.badReponse = reponseReturn.data.reponse;
			}
		});
	}



}]); // fin dont delete XD
