app.controller('menuCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams) {

	var buttonQuestionColor = [];
	var buttonQuestionDisabled = [];
	var buttonQuestionSpan = [];

	$scope.init = function(){
	try {
		$http.get(config.getApiPath() + '/game/'+$rootScope.game.id).then(function(game) {

			if ($rootScope.team) {
				$http.get(config.getApiPath() + '/game/' + $rootScope.game.id + '/' + $rootScope.team.id).then(function(question) {
					$rootScope.questions = question.data;
					$rootScope.timerGame($rootScope.game.id,$rootScope.team.id);
					for (var i = 0; i < $rootScope.questions.length; i++) {
						if ($rootScope.questions[i].enabled == false) {
							buttonQuestionColor.insert(i, 'btn-danger');
							buttonQuestionDisabled.insert(i, true);
							buttonQuestionSpan.insert(i, 'fa fa-ban');

						} else if ($rootScope.questions[i].ended == true) {
							buttonQuestionColor.insert(i, 'btn-success');
							buttonQuestionDisabled.insert(i, false);
							buttonQuestionSpan.insert(i, 'fa fa-check');
						} else {
							buttonQuestionColor.insert(i, 'btn-info');
							buttonQuestionDisabled.insert(i, false);
							buttonQuestionSpan.insert(i, 'fa fa-question');
						}
					}
					$scope.buttonQuestionColor = buttonQuestionColor;
					$scope.buttonQuestionDisabled = buttonQuestionDisabled;
					$scope.buttonQuestionSpan = buttonQuestionSpan;
				});
			}
		});
	} catch (e) {

	}
	}
	$scope.init();

	if (!$rootScope.game) {
		$scope.init();
	}

	if (!$rootScope.goodAnswers) {
		$rootScope.goodAnswers = {};
	}
	//if no teamname return home
	if (!$rootScope.teamName) {
		$state.go('app.home');
	}

	if ($rootScope.stopTimerHints) {
		$rootScope.stopTimerHints();
	}

	Array.prototype.insert = function(index, item) {
		this.splice(index, 0, item);
	};

	$scope.question = function(id_question) {
		$rootScope.id_question = id_question;
		$http.post(config.getApiPath() + '/tqt/' + $rootScope.team.id + '/' + id_question).then(function(question) {
			$state.go('app.question');
		});
	}

	$rootScope.reload = function () {
		$scope.init();
	}
}]); // fin dont delete XD
