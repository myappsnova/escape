app.controller('homeCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams) {

	document.body.style = "background: url('images/background.jpg') no-repeat center center fixed; background-size: cover;";
	$rootScope.timeColor = 'black';

	$rootScope.reloadRoute = function() {
		location.reload();
	}

	$rootScope.questions = '';
	try {
		$scope.teamName = $rootScope.one.teamName;
		if ($scope.teamName) {
			$scope.testing = true;
		}
	} catch (e) {

	}

	$scope.understand = function() {
		$rootScope.endBackCode = '';
		if (!$scope.teamName) {
			$scope.cheat = 'Pas bien de trifouiller le code!!';
		} else {
			$rootScope.showGameList = false;
			$scope.registerTeam = function() {
				var data = {
					teamName: $scope.teamName,
					game: $rootScope.game,
					id_game: $rootScope.game.id
				};
				$http.post(config.getApiPath() + '/start', data).then(function(response) {
					$rootScope.teamName = $scope.teamName;
					$rootScope.team = response.data;
					$rootScope.timerGame($rootScope.game.id,$rootScope.team.id);
					$state.go('app.menu');
				}, function(x) {
					if (x.status === 409) {
						if (x.data.error.message == 'Team name already used') {
							$scope.error = 'Le nom d\'équipe est déja utilisé.';
						}
					}
				})
			}
			$scope.registerTeam();
			$rootScope.showThanks($rootScope.game.id);
		}
	}




}]); // fin dont delete XD
