app.controller('indexCtrl', ["config","$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval) {

	$scope.showPanel = false;
	$scope.showCommingSoon = true;

	$http.delete(config.getApiPath() + '/admin/tester').then(function() {
	})

	try {
		var one = tester;
	} catch (e) {

	}

	if (one) {
		$rootScope.one = one;
		$scope.showPanel = true;
		$scope.showCommingSoon = false;
	}

	$scope.init = function () {
		// $http.get(config.getApiPath() + '/version').then(function(version) {
		// 	$scope.version  = version.data;
		// })

		$rootScope.endBackCode = '';
		$http.get(config.getApiPath() + '/game').then(function(games) {
			games = games.data;
			$scope.gameList = games;

			if (games == 'no game') {
				$scope.showPanel = false;
				$scope.showCommingSoon = true;
			}else if (games.length == 1) {
					$rootScope.getGame(games[0].id);
					$rootScope.showGameList = false;
					$scope.showPanel = true;
					$scope.showCommingSoon = false;
			}else {
					$rootScope.showGameList = true;
					$scope.showPanel = true;
					$scope.showCommingSoon = false;
				for (var i = 0; i < games.length; i++) {
					if (games[i].default_game == 1) {
						$rootScope.getGame(games[i].id);
						$scope.selectedQ = games[i].id;
					}

				}
			}

			if ($rootScope.one) {
				$http.get(config.getApiPath() + '/game/'+one.game).then(function(game) {
					$rootScope.game = game.data;
					var distance = $rootScope.game.time * 1000;
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = hours + "h " + minutes + "m " + seconds + "s ";
					$scope.selectedQ = one.game;
					$rootScope.showGameList = false;
				})
				$rootScope.teamName = $rootScope.one.teamName;
			}

		})
	}
	$scope.init();

	$rootScope.timeColor = 'black';
	$rootScope.intervals = [];

	$rootScope.getGame = function(id_game){
		$rootScope.endBackCode = '';

		$http.get(config.getApiPath() + '/game/'+id_game).then(function(game) {
			$rootScope.stopTimer();
			$rootScope.stopTimerHints();
			delete $rootScope.teamName;
			delete $rootScope.team;

			if (game.data.length == 0) {
					$rootScope.game = 0;
			}else {
					$rootScope.game = game.data;
					var distance = $rootScope.game.time * 1000;
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = hours + "h " + minutes + "m " + seconds + "s ";

			}
		})

	}

	if ($rootScope.teamName == undefined) {
		$rootScope.teamName ="";
		$state.go('app.home');
	}else

	if ($rootScope.stopTimerHints) {
		$rootScope.stopTimerHints();
	}

	var stop;
	$rootScope.timerGame = function(id_game,id_team) {
		if ( angular.isDefined(stop) ) return;
		stop = $interval(function() {
			$http.get(config.getApiPath() + '/check/time/'+id_game+'/'+ id_team).then(function(time) {
				$rootScope.active = time.data;
				if (!$rootScope.questions) {
					$rootScope.reload();
				}
				if (!time.data.active) {
					if (time.data.left === false) {
						$rootScope.stopTimer();
						$scope.timer = "Partie déjà finie!";
						delete $rootScope.questions;
						$state.go('app.end');
					}else {
						$rootScope.stopTimer();
						$scope.timer = "Out of time";
						document.body.style = "background: url('images/timeout.jpg') no-repeat center center fixed; background-size: cover;";

						$http.patch(config.getApiPath() + '/stop/' + $rootScope.team.id).then(function(message) {
							$rootScope.ended = message.data;
							delete $rootScope.questions;
							$state.go('app.end');
						});
					}

				}else if (time.data.left < 10) {
					$scope.timing = 'flip animated';
					$rootScope.timeColor = 'crimson ';
					var distance = (time.data.left * 1000);
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = seconds + "s ";
				}else if (time.data.left < 20) {
					//clean timing session
					$scope.timing = '';
					var distance = (time.data.left * 1000);
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = seconds + "s ";
				}else if (time.data.left < 30) {
					$scope.timing = 'flip animated';
					$rootScope.timeColor = 'OrangeRed ';
					var distance = (time.data.left * 1000);
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = seconds + "s ";
				}else if (time.data.left < 50) {
					//clean timing session
					$scope.timing = '';
					var distance = (time.data.left * 1000);
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = seconds + "s ";
				}else if (time.data.left < 60) {
					$scope.timing = 'flip animated';
					$rootScope.timeColor = 'DarkGoldenRod';
					var distance = (time.data.left * 1000);
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = seconds + "s ";
				}else if (time.data.left < 3600) {
					var distance = (time.data.left * 1000);
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = minutes + "m " + seconds + "s ";
				}else{
					var distance = (time.data.left * 1000);
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					$scope.timer = hours + "h " + minutes + "m " + seconds + "s ";
				}
			});
		}, 1000);
	}
	$rootScope.stopTimer = function(forced = false) {

				if (angular.isDefined(stop)) {
					$interval.cancel(stop);
					stop = undefined;
				}
				if ($rootScope.team && forced == true) {
						$http.patch(config.getApiPath() + '/stop/' + $rootScope.team.id + '/true').then(function(message) {
						$rootScope.ended = message.data;
						$scope.timer = "Game Over";
						$state.go('app.end');
						});
				}
	};

	$rootScope.stopTimerHints = function() {
				if (angular.isDefined(stop)) {
					$interval.cancel(stop);
					stop = undefined;
				}
	};


	$scope.home = function(){
		$rootScope.reloadRoute();
	}

	$scope.menu = function(){
		$state.go('app.menu');
	}
	$scope.exit = function(){
		delete $rootScope.questions;
		delete $rootScope.teamName;
		$rootScope.stopTimer(true);
		$rootScope.stopTimerHints();
	}

	$scope.backToParty = function(code){
		$rootScope.endBackCode = '';
		$rootScope.backCode = code;
		$http.get(config.getApiPath() + '/back/'+ code).then(function(data) {
			data = data.data;
			if (data.status != false) {
				$rootScope.showGameList = false;
				$rootScope.team = data;
				$rootScope.teamName = $rootScope.team.name;
				$rootScope.goodAnswers = data.answers;
				var test = Object.values(data['hint']);
				$rootScope.hintsShow = test[0];
				$state.go('app.menu');
			}else {
				switch (data.data.raison) {
					case 'aborted':
						$rootScope.endBackCode = 'car vous avez abandonner la partie!'
						break;

					case 'timeout':
						$rootScope.endBackCode = 'car votre temps est epuisé'
						break;

					case 'success':
						$rootScope.endBackCode = 'car vous avez déja terminer la partie.'
						break;
				}

			}

		})

	}
	$scope.thanks = {"thx":"nothing"};

	$rootScope.showThanks = function(id_game) {
		$http.get(config.getApiPath() + '/fade/'+id_game).then(function(ret) {

			$scope.thanksList= ret.data;

			$scope.i = 0;
			$scope.thanks = $scope.thanksList[0];
			$scope.max = Object.keys($scope.thanksList).length;

			$scope.fades ="active";
			var top;
			$scope.fade = function() {
				if ( angular.isDefined(top) ) return;
				top = $interval(function() {
					if ($scope.fades == "active")
					{
						$scope.fades ="";
					}else {
						$scope.fades ="active";
						if ($scope.i == $scope.max-1) {
							$scope.i = 0;
							$scope.thanks = $scope.thanksList[0];
						}else {
							$scope.i = $scope.i + 1;
							$scope.thanks = $scope.thanksList[$scope.i];
						}

					}
				},2000);
			}
			$scope.fade();
		})
	}

}]); // fin dont delete XD
