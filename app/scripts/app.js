'use strict';

var app = angular.module('Sherlock', ['ui.router', 'ngStorage', 'ngAnimate','ngTable'])


	.config(['$httpProvider', function($httpProvider) {
		$httpProvider.interceptors.push(['$q', '$injector', '$localStorage', function($q, $injector, $localStorage) {
			return {

				'request': function(config) {
					// define current timestamp
					var ts = moment().format("X");

					var raw = "";
					var url = "";

					if (config.data != undefined) {
						if (config.data instanceof Blob) {
							var a = new FileReader();
							a.readAsBinaryString(config.data);
							a.onloadend = function() {
								raw = a.result;
							};
						} else if (config.data instanceof FormData) {} else {
							raw = angular.toJson(config.data);
						}
					}

					if (config.params != undefined) {

						// alpha ordering like $http
						var keys = Object.keys(config.params).sort(),
							sortedObj = {};

						for (var i in keys) {
							sortedObj[keys[i]] = config.params[keys[i]];
						}
						// ordered array
						config.params = sortedObj;

						var query = [];
						angular.forEach(config.params, function(v, k) {
							if (typeof v === 'string') {
								query.push(k + "=" + encodeURI(v.replace(/ /g, '+')));
							} else {
								query.push(k + "=" + v);
							}
						});
						if (query.length) {
							url = "?" + query.join("&");
						}

					}

					if ($localStorage.app != undefined && $localStorage.secret != undefined) {
						config.headers["X-Application"] = $localStorage.app;
						config.headers["X-Signature"] =
							"$1$" +
							sha1(
								config.method + "+" +
								config.url + url + "+" +
								raw + "+" +
								$localStorage.secret + "+" +
								ts
							);
						config.headers["X-Timestamp"] = ts;
						config.headers["X-Token"] = $localStorage.token;
					}

					return config || $q.when(config);

				},
				'response': function(response) {
					// console.log(response);
					if (response.status == 403 || response.status == 401) {
						$state.go("core.404");
					}
					return response || $q.when(response);
				},
				'responseError': function(rejection) {
					// console.log(rejection);
					if (rejection.status == 401) {
						$injector.invoke(['$state', function($state) {
							$state.go("core.404");
						}]);
					}
					return $q.reject(rejection);
				}
			};
		}]);
	}])

	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.otherwise('/app/home');

		$stateProvider

			//app core pages (errors)
			.state('core', {
				abstract: true,
				url: '/core',
				template: '<div ui-view></div>'
			})
			//app users
			.state('app', {
				abstract: true,
				url: '/app',
				templateUrl: 'views/index.html',
				controller: 'indexCtrl'
			})

			//////////////////////////////////////////////////////
			//404
			.state('core.404', {
				url: '/404',
				templateUrl: '404.html'
			})

			//Landing page
			.state('app.home', {
				url: '/home',
				templateUrl: 'views/pages/home.html',
				controller: 'homeCtrl'
			})
			.state('app.menu', {
				url: '/menu',
				templateUrl: 'views/pages/menu.html',
				controller: 'menuCtrl'
			})
			.state('app.question', {
				url: '/question',
				templateUrl: 'views/pages/question.html',
				controller: 'questionCtrl'
			})
			.state('app.end', {
				url: '/end',
				templateUrl: 'views/pages/end.html',
				controller: 'endCtrl'
			})

	}])
