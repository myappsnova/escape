-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 04 déc. 2018 à 20:17
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sherlock`
--

-- --------------------------------------------------------

--
-- Structure de la table `access`
--

CREATE TABLE `access` (
  `app` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `team_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `addiction`
--

CREATE TABLE `addiction` (
  `id` int(10) NOT NULL,
  `id_question` int(10) NOT NULL,
  `addiction_id_question` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bad_answers`
--

CREATE TABLE `bad_answers` (
  `id` int(11) NOT NULL,
  `reponse` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bad_answers`
--

INSERT INTO `bad_answers` (`id`, `reponse`) VALUES
(1, 'Mais sérieusement...c\'est pas la bonne réponse!'),
(2, 'Elle est bien bonne celle la... mais c\'est pas la bonne réponse!'),
(3, 'Erreur 303 - La réponse à cette question n\'est pas la bonne.'),
(4, 'Essaie encore!'),
(5, 'Ta déja vu l\'erreur \"418 - I’m a teapot\" ?'),
(6, 'Luke... je ne suis pas la bonne réponse.'),
(7, 'Et la marmotte elle met le chocolat dans le papier d\'alu...'),
(8, 'Mauvaise réponse, wrong answer, falsche Antwort, erantzun okerra, verkeerd antwoord, risposta sbagliata,...\r\n\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

CREATE TABLE `game` (
  `id` int(10) NOT NULL,
  `default_game` tinyint(1) NOT NULL DEFAULT '0',
  `active` smallint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `time` smallint(5) NOT NULL,
  `intro` longtext NOT NULL,
  `menu` longtext NOT NULL,
  `conclusion` longtext NOT NULL,
  `timeout` longtext NOT NULL,
  `aborted` longtext NOT NULL,
  `rules` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `game`
--

INSERT INTO `game` (`id`, `default_game`, `active`, `title`, `time`, `intro`, `menu`, `conclusion`, `timeout`, `aborted`, `rules`) VALUES
(6, 1, 1, 'qsdfqsdfqsd', 1, 'sdfg', 'sa\nz\ne\nr\nt\ndf', 'sdfgs', 'gs', 'sdfgs', 'sgsdf'),
(8, 0, 0, 'qsdfsfqs', 1, 'sdf', 'gsdf', 'g', 'sg', 'sdg', 'gf');

-- --------------------------------------------------------

--
-- Structure de la table `game_bad_answers`
--

CREATE TABLE `game_bad_answers` (
  `id` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_bin NOT NULL,
  `reponse` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `hint`
--

CREATE TABLE `hint` (
  `id` int(10) NOT NULL,
  `id_question` int(10) NOT NULL,
  `hint` varchar(255) NOT NULL,
  `time` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `hint`
--

INSERT INTO `hint` (`id`, `id_question`, `hint`, `time`) VALUES
(1, 73, '1', 1),
(2, 75, '4564', 1),
(3, 73, '2', 2);

-- --------------------------------------------------------

--
-- Structure de la table `nearby`
--

CREATE TABLE `nearby` (
  `id` int(10) NOT NULL,
  `id_question` int(10) NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `near_answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `id` int(10) NOT NULL,
  `id_game` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `strict` tinyint(1) NOT NULL DEFAULT '0',
  `nearby` tinyint(1) NOT NULL DEFAULT '0',
  `hint` tinyint(1) NOT NULL DEFAULT '0',
  `addiction` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '2',
  `text` longtext,
  `file` varchar(255) DEFAULT NULL,
  `trouble` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id`, `id_game`, `title`, `question`, `reponse`, `strict`, `nearby`, `hint`, `addiction`, `type`, `text`, `file`, `trouble`) VALUES
(73, 6, 'a', 'a', 'a', 0, 0, 1, 0, 2, 'a', '', 'aa'),
(74, 6, 'z', 'z', 'z', 0, 0, 0, 0, 2, 'z', '', 'zz'),
(75, 6, 'e', 'e', 'e', 0, 0, 1, 0, 2, 'e', '', 'ee'),
(76, 6, 'r', 'r', 'r', 0, 0, 0, 0, 2, 'r', '', 'rr'),
(77, 6, 't', 't', 't', 0, 0, 0, 0, 2, 't', '', 'tt');

-- --------------------------------------------------------

--
-- Structure de la table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `question_bad_answers`
--

CREATE TABLE `question_bad_answers` (
  `id` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_bin NOT NULL,
  `reponse` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `team`
--

CREATE TABLE `team` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_game` int(10) NOT NULL,
  `begin` int(10) DEFAULT NULL,
  `end` int(10) DEFAULT NULL,
  `end_type` varchar(20) DEFAULT NULL,
  `code` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `team`
--

INSERT INTO `team` (`id`, `name`, `id_game`, `begin`, `end`, `end_type`, `code`) VALUES
(166, 'a', 6, 1543756152, 1543756164, 'aborted', '2ntFrUY5'),
(167, 'z', 6, 1543756249, 1543756263, 'success', 'qtebexyZ'),
(168, 'e', 6, 1543756278, 1543756293, 'success', 'uFveE88g'),
(169, 'r', 6, 1543756336, 1543756345, 'aborted', 'wZbr853T'),
(170, 'aa', 6, 1543756380, 1543756441, 'time out', 'yvWQgj9g'),
(171, 'zz', 6, 1543756535, 1543756542, 'aborted', 'BDydXe9K');

-- --------------------------------------------------------

--
-- Structure de la table `team_hint`
--

CREATE TABLE `team_hint` (
  `id_team` int(11) NOT NULL,
  `id_hint` int(11) NOT NULL,
  `time` int(10) NOT NULL,
  `penality` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `team_hint`
--

INSERT INTO `team_hint` (`id_team`, `id_hint`, `time`, `penality`) VALUES
(168, 1, 1543756280, 60),
(169, 1, 1543756338, 59),
(169, 3, 1543756339, 118);

-- --------------------------------------------------------

--
-- Structure de la table `team_question_times`
--

CREATE TABLE `team_question_times` (
  `id_question` int(10) NOT NULL,
  `id_team` int(10) NOT NULL,
  `begin` int(10) NOT NULL,
  `end` int(10) DEFAULT NULL,
  `trouble` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `team_question_times`
--

INSERT INTO `team_question_times` (`id_question`, `id_team`, `begin`, `end`, `trouble`) VALUES
(73, 167, 1543756250, 1543756252, 0),
(74, 167, 1543756253, 1543756254, 0),
(75, 167, 1543756256, 1543756257, 0),
(76, 167, 1543756258, 1543756260, 0),
(77, 167, 1543756261, 1543756263, 0),
(73, 168, 1543756280, 1543756282, 0),
(74, 168, 1543756283, 1543756285, 0),
(75, 168, 1543756286, 1543756289, 0),
(76, 168, 1543756290, 1543756291, 0),
(77, 168, 1543756292, 1543756293, 0),
(73, 169, 1543756337, 1543756341, 0),
(74, 169, 1543756341, 1543756343, 0),
(73, 170, 1543756385, 1543756387, 0),
(73, 171, 1543756536, 1543756538, 1),
(74, 171, 1543756538, 1543756540, 0);

-- --------------------------------------------------------

--
-- Structure de la table `thanks`
--

CREATE TABLE `thanks` (
  `id` int(10) NOT NULL,
  `id_game` int(11) DEFAULT NULL,
  `thx` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `thanks`
--

INSERT INTO `thanks` (`id`, `id_game`, `thx`) VALUES
(29, NULL, 'g1'),
(31, 6, 'q1'),
(32, 6, 'q2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `access`
--
ALTER TABLE `access`
  ADD UNIQUE KEY `app` (`app`);

--
-- Index pour la table `addiction`
--
ALTER TABLE `addiction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`),
  ADD KEY `addiction_id_question` (`addiction_id_question`);

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bad_answers`
--
ALTER TABLE `bad_answers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `game_bad_answers`
--
ALTER TABLE `game_bad_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_game` (`id_game`);

--
-- Index pour la table `hint`
--
ALTER TABLE `hint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`);

--
-- Index pour la table `nearby`
--
ALTER TABLE `nearby`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_game` (`id_game`);

--
-- Index pour la table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`);

--
-- Index pour la table `question_bad_answers`
--
ALTER TABLE `question_bad_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_question` (`id_question`);

--
-- Index pour la table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_ibfk_1` (`id_game`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `team_hint`
--
ALTER TABLE `team_hint`
  ADD KEY `team_hint_ibfk_1` (`id_hint`),
  ADD KEY `team_hint_ibfk_2` (`id_team`);

--
-- Index pour la table `team_question_times`
--
ALTER TABLE `team_question_times`
  ADD KEY `team_question_times_ibfk_1` (`id_question`),
  ADD KEY `team_question_times_ibfk_2` (`id_team`);

--
-- Index pour la table `thanks`
--
ALTER TABLE `thanks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_game` (`id_game`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `addiction`
--
ALTER TABLE `addiction`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `bad_answers`
--
ALTER TABLE `bad_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `game`
--
ALTER TABLE `game`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `game_bad_answers`
--
ALTER TABLE `game_bad_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `hint`
--
ALTER TABLE `hint`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `nearby`
--
ALTER TABLE `nearby`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT pour la table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `question_bad_answers`
--
ALTER TABLE `question_bad_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT pour la table `thanks`
--
ALTER TABLE `thanks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `addiction`
--
ALTER TABLE `addiction`
  ADD CONSTRAINT `addiction_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addiction_ibfk_2` FOREIGN KEY (`addiction_id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `game_bad_answers`
--
ALTER TABLE `game_bad_answers`
  ADD CONSTRAINT `game_bad_answers_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `hint`
--
ALTER TABLE `hint`
  ADD CONSTRAINT `hint_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `nearby`
--
ALTER TABLE `nearby`
  ADD CONSTRAINT `nearby_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `question_answers`
--
ALTER TABLE `question_answers`
  ADD CONSTRAINT `question_answers_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `question_bad_answers`
--
ALTER TABLE `question_bad_answers`
  ADD CONSTRAINT `question_bad_answers_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `team_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `team_hint`
--
ALTER TABLE `team_hint`
  ADD CONSTRAINT `team_hint_ibfk_1` FOREIGN KEY (`id_hint`) REFERENCES `hint` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `team_hint_ibfk_2` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `team_question_times`
--
ALTER TABLE `team_question_times`
  ADD CONSTRAINT `team_question_times_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `team_question_times_ibfk_2` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `thanks`
--
ALTER TABLE `thanks`
  ADD CONSTRAINT `thanks_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
