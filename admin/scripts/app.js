'use strict';

var adm = angular.module('Sherlock', ['ui.router', 'ngStorage', 'ngAnimate','ngTable'])


	.config(['$httpProvider', function($httpProvider) {
		$httpProvider.interceptors.push(['$q', '$injector', '$localStorage', function($q, $injector, $localStorage) {
			return {

				'request': function(config) {
					// define current timestamp
					var ts = moment().format("X");

					var raw = "";
					var url = "";

					if (config.data != undefined) {
						if (config.data instanceof Blob) {
							var a = new FileReader();
							a.readAsBinaryString(config.data);
							a.onloadend = function() {
								raw = a.result;
							};
						} else if (config.data instanceof FormData) {} else {
							raw = angular.toJson(config.data);
						}
					}

					if (config.params != undefined) {

						// alpha ordering like $http
						var keys = Object.keys(config.params).sort(),
							sortedObj = {};

						for (var i in keys) {
							sortedObj[keys[i]] = config.params[keys[i]];
						}
						// ordered array
						config.params = sortedObj;

						var query = [];
						angular.forEach(config.params, function(v, k) {
							if (typeof v === 'string') {
								query.push(k + "=" + encodeURI(v.replace(/ /g, '+')));
							} else {
								query.push(k + "=" + v);
							}
						});
						if (query.length) {
							url = "?" + query.join("&");
						}

					}

					if ($localStorage.adm != undefined && $localStorage.secret != undefined) {
						config.headers["X-Application"] = $localStorage.adm;
						config.headers["X-Signature"] =
							"$1$" +
							sha1(
								config.method + "+" +
								config.url + url + "+" +
								raw + "+" +
								$localStorage.secret + "+" +
								ts
							);
						config.headers["X-Timestamp"] = ts;
						config.headers["X-Token"] = $localStorage.token;
					}

					return config || $q.when(config);

				},
				'response': function(response) {
					// console.log(response);
					if (response.status == 403 || response.status == 401) {
						$state.go("core.404");
					}
					return response || $q.when(response);
				},
				'responseError': function(rejection) {
					// console.log(rejection);
					if (rejection.status == 401) {
						$injector.invoke(['$state', function($state) {
							$state.go("core.404");
						}]);
					}
					return $q.reject(rejection);
				}
			};
		}]);
	}])

	.directive('fileModel', ['$parse', function ($parse) {
	    return {
	    restrict: 'A',
	    link: function(scope, element, attrs) {
	        var model = $parse(attrs.fileModel);
	        var modelSetter = model.assign;

	        element.bind('change', function(){
	            scope.$apply(function(){
	                modelSetter(scope, element[0].files[0]);
	            });
	        });
	    }
	   };
	}])

	.service('fileUpload', ['$http', function ($http) {
	    this.uploadFileToUrl = function(file, uploadUrl, name){
	         var fd = new FormData();
	         fd.append('file', file);
	         fd.append('name', name);
	         $http.post(uploadUrl, fd, {
	             transformRequest: angular.identity,
	             headers: {'Content-Type': undefined,'Process-Data': false}
	         })
	         .success(function(){
	            //console.log("Success");
	         })
	         .error(function(){
	            //console.log("Success");
	         });
	     }
	 }])

	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.otherwise('/adm/home');

		$stateProvider

			//adm core pages (errors)
			.state('core', {
				abstract: true,
				url: '/core',
				template: '<div ui-view></div>'
			})
			//adm users
			.state('adm', {
				abstract: true,
				url: '/adm',
				templateUrl: 'views/index.html',
				controller: 'initCtrl'
			})

			//////////////////////////////////////////////////////
			//404
			.state('core.404', {
				url: '/404',
				templateUrl: '404.html'
			})

			//Landing page
			.state('adm.home', {
				url: '/home',
				templateUrl: 'views/pages/home.html',
				controller: 'homeCtrl'
			})
			.state('adm.console', {
				url: '/console',
				templateUrl: 'views/pages/console.html',
				controller: 'consoleCtrl'
			})
			.state('adm.create', {
				url: '/create',
				templateUrl: 'views/pages/create.html',
				controller: 'createCtrl'
			})
			.state('adm.scores', {
				url: '/scores',
				templateUrl: 'views/pages/scores.html',
				controller: 'scoresCtrl'
			})
			.state('adm.party', {
				url: '/party',
				templateUrl: 'views/pages/party.html',
				controller: 'partyCtrl'
			})
			.state('adm.editGame', {
				url: '/edit/game',
				templateUrl: 'views/pages/editGame.html',
				controller: 'editGameCtrl'
			})
			.state('adm.editGamePlus', {
				url: '/edit/game/plus',
				templateUrl: 'views/pages/editGamePlus.html',
				controller: 'editGamePlusCtrl'
			})
			.state('adm.hints', {
				url: '/edit/game/plus/hints',
				templateUrl: 'views/pages/hints.html',
				controller: 'hintsCtrl'
			})
			.state('adm.addictions', {
				url: '/edit/game/plus/addictions',
				templateUrl: 'views/pages/addictions.html',
				controller: 'addictionsCtrl'
			})
			.state('adm.nearby', {
				url: '/edit/game/plus/nearby',
				templateUrl: 'views/pages/nearby.html',
				controller: 'nearbyCtrl'
			})
			.state('adm.thanks', {
				url: '/edit/game/plus/thanks',
				templateUrl: 'views/pages/thanks.html',
				controller: 'thanksCtrl'
			})
			.state('adm.badAnswers', {
				url: '/edit/game/plus/badAnswers',
				templateUrl: 'views/pages/badAnswers.html',
				controller: 'badAnswersCtrl'
			})


	}])
