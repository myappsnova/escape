adm.controller('homeCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams) {

	delete $rootScope.loged;

	$http.get(config.getApiPath() + '/admin/administration').then(function(response) {
		$scope.administration = response.data;
	})

	$scope.create = function(){
		if ($scope.register.password != $scope.register.confirm) {
			$scope.error = 'Les deux mots de passe sont différents';
		}else {
			$http.post(config.getApiPath() + '/admin/create',$scope.register).then(function() {
				$scope.administration = 'login';
				$state.reload();
			})
		}
	}

	$scope.logme = function(){

		$http.post(config.getApiPath() + '/admin/login',$scope.login).then(function(action) {

			if (action.data) {
				$rootScope.loged = true;
				$state.go('adm.console');
			}else {
				$scope.error = action.data;
			}
		}, function(x) {
			if (x.status === 406) {
				if (x.data.error.message == 'Incorrect Login') {
					$scope.error = 'Incorrect Login';
				}else if (x.data.error.message == 'Incorrect Password') {
					$scope.error = 'Incorrect Password';
				}
			}
		})
	}


}]); // fin dont delete XD
