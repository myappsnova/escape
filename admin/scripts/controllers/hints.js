adm.controller('hintsCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval,NgTableParams) {

	$scope.getHint = function() {
		$scope.showSave = false;
		$http.get(config.getApiPath() + '/admin/hints/'+$rootScope.idQuestion).then(function(info_hints) {
			var data = info_hints.data;
			$scope.hints = new NgTableParams({}, {counts: [10,20,30], dataset: data});
		})
	}
	$scope.getHint();

	$scope.deleteHintConfirm = function (state,hint) {
		$scope.confirmDeleteHint = {'hint':hint,'show':state};
	}

	$scope.deleteHint = function(hint) {
		$http.delete(config.getApiPath() + '/admin/hint/'+hint.id).then(function() {
			$scope.getHint();
			$scope.message = "Indice "+ hint.id + " supprimer!";
		})
	}

	$scope.saveHintChanges = function(hints) {
		$scope.message = ""
		$http.patch(config.getApiPath() + '/admin/hints',hints).then(function() {
		$scope.message = "Indices sauvegardées";
		$scope.showSave = false;
		})
	}

	$scope.createHint = function(hint) {
		$http.post(config.getApiPath() + '/admin/hint/'+$rootScope.idQuestion,hint).then(function() {
			$scope.getHint();
			delete $scope.data;
		})
	}

	$scope.infoChange = function () {
		$scope.showSave = true;
	}

	$scope.undoChange = function(data){
		$scope.getHint();
	}

	$scope.back = function(){
		$state.go('adm.editGamePlus');
	}

}]); // fin dont delete XD
