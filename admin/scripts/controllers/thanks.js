adm.controller('thanksCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval,NgTableParams) {

	$scope.getThanks = function(){
		$scope.showSave = false;

		$http.get(config.getApiPath() + '/admin/games').then(function(games) {
			$scope.gamesList=games.data;
		})


		$http.get(config.getApiPath() + '/admin/application/thanks').then(function(thanks) {
			var data = thanks.data;
			$scope.thanks_list = new NgTableParams({}, {counts: [10,20], dataset: data});
		})
	}
	$scope.getThanks();


	$scope.infoChange = function () {
		$scope.showSave = true;
	}

	$scope.deleteThanksConfirm = function (state,thanks) {
		$scope.confirmDeleteThanks = {'thanks':thanks,'show':state};
	}

	$scope.deleteThanks = function(thanks) {
		$http.delete(config.getApiPath() + '/admin/application/thanks/'+thanks).then(function() {
			$scope.getThanks();
			$scope.message = "Supprimer!";
		})
	}

	$scope.saveThanksChanges = function(thanks) {
		$scope.message = ""
		$http.patch(config.getApiPath() + '/admin/application/thanks',thanks).then(function() {
			$scope.message = "Mise a jour OK!";
			$scope.showSave = false;
		})
	}

	$scope.undoChange = function(){
		$scope.getThanks();
	}

	$scope.createThanks = function(thanks) {
		$http.post(config.getApiPath() + '/admin/application/thanks',thanks).then(function() {
			$scope.getThanks();
			delete $scope.data;
		})
	}

}]); // fin dont delete XD
