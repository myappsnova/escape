adm.controller('addictionsCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval,NgTableParams) {

	$scope.getAddictions = function() {
		$http.get(config.getApiPath() + '/admin/addictions/'+$rootScope.idQuestion).then(function(info_hints) {
			var data = info_hints.data;
			$scope.addictions = new NgTableParams({}, {counts: [10,20,30], dataset: data});
		})
		$http.get(config.getApiPath() + '/admin/list/questions/'+$rootScope.idGame+'/'+$rootScope.idQuestion).then(function(questions) {
			$scope.questionList = questions.data;
		})
	}
	$scope.getAddictions();

	$scope.deleteAddictionConfirm = function (state,addiction) {
		$scope.confirmDeleteAddiction = {'addict':addiction,'show':state};
	}

	$scope.deleteAddiction = function(addiction) {
		$http.delete(config.getApiPath() + '/admin/addiction/'+addiction.id).then(function() {
			$scope.getAddictions();
			$scope.message = "Dépendance "+ addiction.id + " supprimer!"
		})
	}

	$scope.createAddiction = function(question) {

		$http.post(config.getApiPath() + '/admin/addiction/'+$rootScope.idQuestion,question).then(function() {
			$scope.getAddictions();
		})
	}
	$scope.getQuestion = function(question){
		$scope.selectedQuestion = question;
	}
	$scope.back = function(){
		$state.go('adm.editGamePlus');
	}


}]); // fin dont delete XD
