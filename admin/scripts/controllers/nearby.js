adm.controller('nearbyCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval,NgTableParams) {

	$scope.getNearbys = function() {
			$scope.showSave = false;
		$http.get(config.getApiPath() + '/admin/nearby/'+$rootScope.idQuestion).then(function(info_nearby) {
			var data = info_nearby.data;
			$scope.nearbys = new NgTableParams({}, {counts: [10,20,30], dataset: data});
		})
	}
	$scope.getNearbys();

	$scope.deleteNearbyConfirm = function (state,nearby) {
		$scope.confirmDeleteNearby = {'nearby':nearby,'show':state};
	}

	$scope.deleteNearby = function(nearby) {
		$http.delete(config.getApiPath() + '/admin/nearby/'+nearby.id).then(function() {
			$scope.getNearbys($rootScope.idQuestion);
			$scope.message = "Approximatif "+ nearby.id + " supprimer!";
		})
	}

	$scope.saveNearbyChanges = function(nearbys) {
		$scope.message = ""
		$http.patch(config.getApiPath() + '/admin/nearbys',nearbys).then(function() {
		$scope.message = "Réponses approximatives ont été mise à jour!";
		$scope.showSave = false;
		})
	}

	$scope.createNearby = function(nearby) {
		$http.post(config.getApiPath() + '/admin/nearby/'+$rootScope.idQuestion,nearby).then(function() {
			$scope.getNearbys();
			delete $scope.data;
		})
	}

	$scope.infoChange = function () {
		$scope.showSave = true;
	}

	$scope.undoChange = function(){
		$scope.getNearbys();
	}

	$scope.back = function(){
		$state.go('adm.editGamePlus');
	}

}]); // fin dont delete XD
