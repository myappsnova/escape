adm.controller('initCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval) {

	$scope.home = function(){
		$state.go('adm.console');
	}

	$scope.party = function(){
		$state.go('adm.party');
	}

	$scope.scores= function	(){
		$state.go('adm.scores');
	}

	$scope.thanks = function	(){
		$state.go('adm.thanks');
	}

	$scope.badAnswers = function	(){
		$state.go('adm.badAnswers');
	}

	if ($rootScope.loged == undefined) {
		$state.go('adm.home');
	}

	// $http.get(config.getApiPath() + '/version').then(function(version) {
	// 	$scope.version  = version.data;
	// })

}]); // fin dont delete XD
