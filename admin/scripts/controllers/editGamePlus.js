adm.controller('editGamePlusCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams","fileUpload", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval, NgTableParams,fileUpload) {

	$scope.showSave = [];
	$scope.init = function () {
		$http.get(config.getApiPath() + '/admin/questions/'+$rootScope.idGame).then(function(info_questions) {
			var data = info_questions.data;
			$scope.questions = new NgTableParams({}, {counts: [10,20], dataset: data});
		})
	}
	$scope.init();

	//question
	$scope.deleteQuestionConfirm = function (state,id_question) {
		$scope.confirmDeleteQuestion = {'question':id_question,'show':state};
	}

	$scope.infoChange = function (id_question) {
		$scope.showSave[id_question] = true;
	}
	$scope.undoChange = function(id_question){
		$scope.showSave[id_question] = false;
		$scope.init();
	}

	$scope.deleteQuestion = function(id_question) {
		$scope.message = ""
		$http.delete(config.getApiPath() + '/admin/questions/'+id_question).then(function() {
			$scope.init();
			$scope.message = "Question "+ id_question + " supprimer!"
		})
	}

	$scope.saveQuestionChanges = function(question) {
		$scope.message = ""
		$scope.showSave[question.id] = false;
		$http.patch(config.getApiPath() + '/admin/questions',question).then(function() {
			$scope.message = "Question "+ question.id + " a été mit à jour!"
		})
	}

	$scope.createQuestion = function(question) {
		console.log(question);
		if ($scope.myFile === undefined) {
			question.type = 2;
		}

		$http.post(config.getApiPath() + '/admin/questions/'+$rootScope.idGame,question).then(function(answer) {
			if ($scope.myFile) {
				var file = $scope.myFile;
				var uploadUrl = config.getApiPath() + '/file/'+answer.data.id;
				fileUpload.uploadFileToUrl(file, uploadUrl);
			}
				$scope.init();
		});
	}

	//hint
	$scope.hint = function(id_question){
		$rootScope.idQuestion = id_question;
		$state.go('adm.hints');
	}
	//addiction
	$scope.addiction = function(id_question){
		$rootScope.idQuestion = id_question;
		$state.go('adm.addictions');
	}
	//nearby
	$scope.nearby = function(id_question){
		$rootScope.idQuestion = id_question;
		$state.go('adm.nearby');
	}

	//other
	$scope.close = function(id_question){
		$scope.message = ""
		$scope.showHint = false;
		$scope.showAddiction = false;
		$scope.showNearby = false;
		$scope.showSomething = true;
	}
	$scope.close();

	$scope.back = function(){
		$state.go('adm.party');
	}

	$scope.upload = function(id_question){
			$rootScope.idQuestion = id_question;
			delete $scope.myFile;
	}
	$scope.updateUpload = function(question){
		$scope.message = "";
		var file = $scope.myFile;
		var uploadUrl = config.getApiPath() + '/file/'+$rootScope.idQuestion;
		fileUpload.uploadFileToUrl(file, uploadUrl);
		$scope.message = "Fichier uploader!"
		$scope.saveQuestionChanges(question);
		$scope.showSave[question.id] = false;
		$scope.init();
	}

	$scope.clearNew = function(){
		delete $scope.data;
		delete $scope.myFile;
	}




}]); // fin dont delete XD
