
adm.controller('editGameCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval, NgTableParams) {


	$http.get(config.getApiPath() + '/admin/game/'+$rootScope.idGame).then(function(game) {
		$scope.game = game.data;
	})

	$scope.saveGameChanges = function() {
		$http.patch(config.getApiPath() + '/admin/game/'+$rootScope.idGame+'/update',$scope.game).then(function(game) {
			$state.go('adm.party')
		})
	}

	$scope.back = function () {
		$state.go('adm.party');
	}

}]); // fin dont delete XD
