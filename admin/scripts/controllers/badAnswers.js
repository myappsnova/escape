adm.controller('badAnswersCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval,NgTableParams) {

	$scope.getbadAnswers = function(){
		$scope.showSave = false;
		$http.get(config.getApiPath() + '/admin/application/bad_answers').then(function(bad_answers) {
			var data = bad_answers.data;
			$scope.bad_answers = new NgTableParams({}, {counts: [10,20], dataset: data});
		})
	}
	$scope.getbadAnswers();


	$scope.infoChange = function () {
		$scope.showSave = true;
	}

	$scope.deleteBadAnswersConfirm = function (state,badAnswers) {
		$scope.confirmDeleteBadAnswers = {'badAnswers':badAnswers,'show':state};
	}

	$scope.deletebadAnswers = function(badAnswers) {
		$http.delete(config.getApiPath() + '/admin/application/bad_answers/'+badAnswers).then(function() {
			$scope.getbadAnswers();
			$scope.message = "Supprimer!";
		})
	}

	$scope.saveBadAnswersChanges = function(badAnswers) {
		$scope.message = ""
		$http.patch(config.getApiPath() + '/admin/application/bad_answers',badAnswers).then(function() {
			$scope.message = "Mise a jour OK!";
			$scope.showSave = false;
		})
	}

	$scope.undoChange = function(){
		$scope.getbadAnswers();
	}

	$scope.createBadAnswers = function(badAnswers) {
		$http.post(config.getApiPath() + '/admin/application/bad_answers',badAnswers).then(function() {
			$scope.getbadAnswers();
			delete $scope.data;
		})
	}

}]); // fin dont delete XD
