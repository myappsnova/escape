adm.controller('partyCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "$interval","NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, $interval, NgTableParams) {

	$scope.init = function(){
		$http.get(config.getApiPath() + '/admin/games').then(function(info_games) {
			var data = info_games.data;
			$scope.games = new NgTableParams({}, {counts: [10,20,30], dataset: data});
		})
	}
	$scope.init();

	$scope.active = function(id_game){
		$scope.stopMessage();
		$http.patch(config.getApiPath() + '/admin/games/1/'+id_game).then(function() {
			$scope.init();
		})
	}

	$scope.desactive = function(id_game){
		$scope.stopMessage();
		$http.patch(config.getApiPath() + '/admin/games/0/'+id_game).then(function() {
			$scope.init();
		})
	}

	$scope.activeDefault = function(id_game){
		$scope.stopMessage();
		$http.patch(config.getApiPath() + '/admin/default/games/1/'+id_game).then(function() {
			$scope.init();
		})
	}

	$scope.desactiveDefault = function(id_game){
		$scope.stopMessage();
		$http.patch(config.getApiPath() + '/admin/default/games/0/'+id_game).then(function() {
			$scope.init();
		})
	}

	$scope.deleteGame = function(id_game,title){
		$scope.stopMessage();
		$http.delete(config.getApiPath() + '/admin/game/'+id_game).then(function() {
			$scope.init();
			$scope.message = "Partie "+ id_game + ' - ' + title + " supprimer!";
		})
	}

	$scope.deleteGameConfirm = function (state,id_game) {
		$scope.confirmDeleteGame = {'game':id_game,'show':state};
	}


	$scope.editGame = function(id_game){
		$rootScope.idGame = id_game;
		$state.go('adm.editGame')
	}

	$scope.editGamePlus = function(id_game){
		$rootScope.idGame = id_game;
		$state.go('adm.editGamePlus')
	}

	$scope.create = function(){
		$state.go('adm.create')
	}

	$scope.clone = function(id_game){
		$scope.stopMessage();
		$http.post(config.getApiPath() + '/admin/game/clone/'+id_game).then(function() {
			$scope.init();
		})
		$scope.message = "Partie et questions uniquement ont été copier!";
		$scope.redMessage = "Les dépendances,indices et approximatif des questions n'ont pas été copier!";
	}
	$scope.stopMessage = function (id_game) {
		delete $scope.message;
		delete $scope.redMessage;
	}

	$scope.testGame = function	(id_game){
		// Store the return of the `open` command in a variable
		var url = config.getAppPath() + "/#/app/home";
		var newWindow = window.open(url);
		// Access it using its variable
		var data = {"teamName":"TESTER","game":id_game};
		newWindow.tester = data;

	}

}]); // fin dont delete XD
