adm.controller('scoresCtrl', ["config", "$scope", "$rootScope", "$state", "$http", "$localStorage", "$stateParams", "NgTableParams", function(config, $scope, $rootScope, $state, $http, $localStorage, $stateParams, NgTableParams) {

	$http.get(config.getApiPath() + '/admin/list/games/1').then(function(games) {
		$scope.getScores(games.data);
		$scope.selectedGameDetail = games.data;
	});

	$scope.getScores = function(selectedGame){
		$http.get(config.getApiPath() + '/admin/test/'+selectedGame.id).then(function(scores) {
			var data = scores.data;
			$scope.scoreTable = new NgTableParams({}, {counts: [10,25], dataset: data});
			$scope.getTitle(selectedGame.id);
		});

	}

	$scope.getTitle = function(idGame){
		$http.get(config.getApiPath() + '/admin/list/games/1/'+idGame).then(function(games) {
			$scope.selectedGameDetail = games.data;
		});
	}

	$scope.deleteTeams = function(id_game){
		$http.delete(config.getApiPath() + '/admin/teams/'+id_game).then(function() {
			$scope.message = "Toutes les équipes de la partie "+ id_game + " ont été supprimer!"
			delete $scope.confirmDeleteTeams;
			$scope.test = {'id':id_game};
			$scope.getScores($scope.test);
		})
	}
	$scope.deleteUglyTeams = function(id_game){
		$http.delete(config.getApiPath() + '/admin/ugly/teams/'+id_game).then(function() {
			$scope.message = "Nettoyage equipes inutiles de la partie  "+ id_game + " effectué!"
		})
	}
	$scope.deleteTeamConfirm = function (state,id_game) {
		$scope.confirmDeleteTeams = {'game':id_game,'show':state};
	}

	$http.get(config.getApiPath() + '/admin/list/games').then(function(games) {
		$scope.gameList = games.data;
	});

	$scope.questionScores = function (id_team){
		$http.get(config.getApiPath() + '/admin/scores/question/'+id_team).then(function(questionScores) {
			var data = questionScores.data;
	  		$scope.questionScore = new NgTableParams({}, {counts: [10,20], dataset: data});
		});

	}
}]); // fin dont delete XD
